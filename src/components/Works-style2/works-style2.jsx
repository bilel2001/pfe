import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import initIsotope from '../../common/initIsotope';
import axios from 'axios';

const WorksStyle2 = ({ grid, hideFilter, filterPosition }) => {
  const [allEvents, setAllEvents] = useState([]);
  const [filteredEvents, setFilteredEvents] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const instance = axios.create({ baseURL: 'http://localhost:3001' });
        delete instance.defaults.headers.common['Authorization'];
        const domainevent = '';
        const { data } = await instance.get('api/events/domain', { params: { domainevent } });
        setAllEvents(data);
        setFilteredEvents(data);
        setTimeout(() => {
          initIsotope();
        }, 1000);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  const handleFilter = (domain) => {
    if (domain === 'all') {
      setFilteredEvents(allEvents);
    } else {
      const filtered = allEvents.filter((event) => event.domainevent === domain);
      setFilteredEvents(filtered);
    }
  };

  return (
    <section className={`${grid ? (grid === 3 ? 'three-column' : null) : null} portfolio section-padding pb-70`}>
      {!hideFilter && (
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8 col-md-10">
              <div className="sec-head text-center">
                <h6 className="wow fadeIn" data-wow-delay=".5s">
                  Découvrez Nos Événements
                </h6>
                <h3 className="wow color-font">
                  Nos Événements Récents &amp; <br /> Quelques Événements Passés.
                </h3>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className={`filtering ${filterPosition === 'center' ? 'text-center' : filterPosition === 'left' ? 'text-left' : 'text-right'} col-12`}>
                <div className="filter">
                  <span data-filter="*" className="active" onClick={() => handleFilter('all')}>
                    All
                  </span>
                  <span data-filter=".ecommerce" onClick={() => handleFilter('ecommerce')}>
                    E-commerce
                  </span>
                  <span data-filter=".marketing" onClick={() => handleFilter('marketing')}>
                    Marketing
                  </span>
                  <span data-filter=".digital" onClick={() => handleFilter('digital')}>
                    Digital
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      <div className="container">
        <div className="row">
          <div className="gallery full-width">
            {filteredEvents.map((event, index) => (
              <div
                key={index}
                className={`${
                  grid === 3 ? 'col-lg-4 col-md-6' : grid === 2 ? 'col-md-6' : 'col-12'
                } items ${event.domainevent} wow fadeInUp`}
                data-wow-delay=".4s"
              >
                <div className="item-img">
                  <Link href={`/evenements/${event._id}`}>
                    <a className="imago wow">
                    <img src={`http://localhost:3001/uploads/${event.photoevent}`} alt="Uploaded Photo" />
                      <div className="item-img-overlay"></div>
                    </a>
                  </Link>
                </div>
                <div className="cont">
                  <h6>{event.title}</h6>
                  <a className="date">
                    <span>
                      <i>{event.dated}</i> {event.datef}
                    </span>
                  </a>
                  <span>/</span>
                  <a className="tag">
                    <span>{event.domainevent}</span>
                  </a>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default WorksStyle2;