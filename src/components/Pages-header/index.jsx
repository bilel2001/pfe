/* eslint-disable @next/next/no-img-element */
import React from 'react'

const PagesHeader = () => {
  return (
    <header className="pages-header circle-bg valign">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <div className="cont mt-100 mb-50 text-center">
              <h2 className="color-font fw-700">
              Une plateforme intelligente à gérer
Tous les créateurs de votre marque
              </h2>
              <p>Influenceurs, athlètes, créateurs, affiliés, journalistes, clients, ambassadeurs - les marques gagnantes d'aujourd'hui ont toutes une chose en commun - un vaste réseau de défenseurs partageant leur histoire dans la vie réelle et sur les réseaux sociaux.</p>
            </div>
          </div>
          <div className="col-lg-10">
            <div className="img">
              <img src="/img/slid/about.jpg" alt="" />
            </div>
          </div>
        </div>
      </div>
      <div className="half sub-bg">
        <div className="circle-color">
          <div className="gradient-circle"></div>
          <div className="gradient-circle two"></div>
        </div>
      </div>
    </header>
  );
};

export default PagesHeader;