import React from "react";
import ContactFromDate from "../../data/sections/form-info.json";
import { Formik, Form, Field } from "formik";
import Link from "next/link";

const ContactForm = () => {
  const messageRef = React.useRef(null);
  const sendMessage = (ms) => new Promise((r) => setTimeout(r, ms));
  return (
    <section className="contact section-padding" style={{
      width: '950px',
      paddingRight: '100px',
      paddingLeft: '10px',

      marginRight: '100px',
      marginLeft: '335px',
    }} >
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Modifier le profil.</h4>
              <Formik
                initialValues={{
                  nom: "",
                  prenom: "",
                  email: "",
                  
                  Domaine: "",
                  Genre: null
                }}
                onSubmit={async (values) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));
                  // show message

                  messageRef.current.innerText =
                    "Your Message has been successfully sent. I will contact you soon.";
                  // Reset the values
                  values.nom = "";
                  values.values.Prénom = "";
                  values.email = "";
                  values.Domaine = "";
                  values.Genre = "";
                 
                  // clear message
                  setTimeout(() => {
                    messageRef.current.innerText = ''
                  }, 2000)
                }}
              >
                {({ errors, touched }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                      <div className="form-group">
                        <Field
                          id="form_name"
                          type="text"
                          name="nom"
                          placeholder="Nom"

                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="prénom"
                          type="text"
                          name="prénom"
                          placeholder="Prénom"

                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="Domaine"
                          type="text"
                          name="Domaine "
                          placeholder="Domaine"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="form_email"
                          type="email"
                          name="email"
                          placeholder="Email"
                        />
                        {errors.email && touched.email && (
                          <div>{errors.email}</div>
                        )}
                      </div>
                    </div>
                    <div className="form-group">
                        <label>Genre:</label>
                        <div style={{ display: "flex", flexDirection: "row-reverse" }}>
                          <div style={{ marginLeft: "10px" }}>
                            <label>
                              <Field type="radio" name="genre" value="femme" />
                              Femme
                            </label>
                          </div>
                          <div>
                            <label>
                              <Field type="radio" name="genre" value="homme" />
                              Homme
                            </label>
                          </div>
                        </div></div>
                   


                  </Form>
                )}
              </Formik>
            </div>
          </div>
          <div className="col-lg-6" style={{
            width: '950px',
            paddingRight: '0px',
            paddingLeft: '10px',
            marginTop: '-390px',
            marginRight: '0px',
            marginLeft: '500px',
          }}>
            <div className="form md-mb50">

              <Formik
                initialValues={{
                  Pays: "",
                  date: "",
                  photo: "",
                  numéro_de_téléphone: "",
                  Description: ""
                }}
                onSubmit={async (values) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));
                  // show message

                  messageRef.current.innerText =
                    "Your Message has been successfully sent. I will contact you soon.";
                  // Reset the values
                  
                  values.Pays = "";
                  values.date= "",
                  values.photo= "",
                  values.numéro_de_téléphone= "",
                  values.Description = "";
                  // clear message
                  setTimeout(() => {
                    messageRef.current.innerText = ''
                  }, 2000)
                }}
              >
                {({ errors, touched }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                      <div className="form-group">
                        <Field
                          id="Pays"
                          type="text"
                          name="Pays"
                          placeholder="Pays"

                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="file"
                          onChange={(event) => {
                            setFieldValue("photo", event.currentTarget.files[0]);
                          }}
                        />
                      </div>
                      <div className="form-group">
                      <Field
                        id="numéro_de_téléphone"
                        type="text"
                        name="numéro de téléphone"
                        placeholder="Numéro de téléphone"
                      />
                    </div>
                      <div className="form-group">
                        <Field
                          id="date"
                          type="date"
                          name="date_naissance"
                          placeholder="Date de naissance"
                        />
                      </div>

                    </div>
                    <div className="form-group">
                        <Field
                          id="Description"
                          type="text"
                          name="Description"
                          placeholder="Description*"

                        />
                      </div>

                    <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                      <span>Enregistrer</span>
                    </button>
                    

                  </Form>
                )}
              </Formik>
            </div>
          </div>
          <div className="col-lg-6"style={{
            
            
            marginTop: ' 50px',
           
            
            
           
           
          }}>
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Mot de passe</h4>
              <Formik
                initialValues={{
                  ancien : "",
                  nouveau: "",
                  nouveau1: "",
                 
                }}
                onSubmit={async (values) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));
                  // show message

                  messageRef.current.innerText =
                    "Your Message has been successfully sent. I will contact you soon.";
                  // Reset the values
                  values.ancien = "";
                  values.nouveau = "";
                  values.nouveau1 = "";
                 
                 
                  // clear message
                  setTimeout(() => {
                    messageRef.current.innerText = ''
                  }, 2000)
                }}
              >
                {({ errors, touched }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                    <div className="form-group">
                        <Field
                          id="ancien"
                          type="password"
                          name="mot_de_passe"
                          placeholder="Ancien Mot de passe"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="nouveau"
                          type="password"
                          name="nouveau"
                          placeholder="nouveau Mot de passe"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="nouveau1"
                          type="password"
                          name="nouveau1"
                          placeholder="Répéter Mot de passe"
                        />
                      </div>
                      
                    </div>
                   
                   
                    <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                      <span>Enregistrer</span>
                    </button>

                  </Form>
                )}
              </Formik>
            </div>
          </div>
          <div className="col-lg-6"style={{
            width: '950px',
            paddingRight: '0px',
            paddingLeft: '10px',
            marginTop: ' -390px',
            marginRight: '0px',
            marginLeft: '500px',
            
            
           
           
          }}>
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Social media</h4>
              <Formik
                initialValues={{
                  Instagram_url: "",
                  Facebook_url: "",
                  Tiktok_url: "",
                  Youtube_url: "",
                 
                }}
                onSubmit={async (values) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));
                  // show message

                  messageRef.current.innerText =
                    "Your Message has been successfully sent. I will contact you soon.";
                  // Reset the values
                  values.Instagram_url = "";
                  values.Facebook_url = "";
                  values.Tiktok_url = "";
                  values.Youtube_url = "";
                 
                 
                  // clear message
                  setTimeout(() => {
                    messageRef.current.innerText = ''
                  }, 2000)
                }}
              >
                {({ errors, touched }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                    
                      <div className="form-group">
                        <Field
                          id="Instagram_url"
                          type="url"
                          name="instagram_url"
                          placeholder="URL de la page Instagram"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="Facebook_url"
                          type="url"
                          name="facebook_url"
                          placeholder="URL de la page Facebook"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="Tiktok_url"
                          type="url"
                          name="tiktok_url"
                          placeholder="URL de la page TikTok"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="Youtube_url"
                          type="url"
                          name="Youtube_url"
                          placeholder="URL de la page Youtube"
                        />
                      </div>
                      
                      
                    </div>
                   
                   
                    <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                      <span>Enregistrer</span>
                    </button>

                  </Form>
                )}
              </Formik>
            </div>
          </div>
      </div>
        </div>
        

    </section>
  );
};

export default ContactForm;
