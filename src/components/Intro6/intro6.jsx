import React from "react";
import Link from "next/link";

const Intro6 = () => {
  return (
    <header className="mobile-app valign">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 valign">
            <div className="caption">
              <h1 className="mb-20">
                Let’s Build Amazing Mobile Apps Together
              </h1>
              <p>
                Sit amet consectetur adipiscing elit, sed do eiusmod tempor
                idunte ut labore et dolore magna aliqua. Quis ipsum suspendisse
                ultrices gravida. Risus commodo viverra maecenas accumsan
              </p>
              
              <div className="butons mt-40">
              <Link href={`/homepage/Sign-up/`}>

                <a  className="butn-gr rounded buton">
                  <span>Influenceur</span>
                 
                     
                </a>
                </Link>
                <Link href={`/homepage/Sign-up-annonceur`}>
                <a  className="butn-bord-dark rounded buton">
                  <span>Annonceur</span>
                  
                </a>
                </Link>
              </div>
            </div>
          </div>

          <div className="col-lg-5 offset-lg-1">
            <div className="img">
              <img src="/img/mobile-app/header-img.png" alt="" />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Intro6;
