/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import appData from "../../data/app.json";
import { handleDropdown, handleMobileDropdown } from "../../common/navbar";

const Navbar = ({ lr, nr, theme }) => {
  return (
    <nav
      ref={nr}
      className={`navbar navbar-expand-lg change ${
        theme === "themeL" ? "light" : ""
      }`}
    >
      <div className="container">
        <Link href="/">
          <a className="logo">
            {theme ? (
              theme === "themeL" ? (
                <img ref={lr} src={appData.darkLogo} alt="logo" />
              ) : (
                <img ref={lr} src={appData.lightLogo} alt="logo" />
              )
            ) : (
              <img ref={lr} src={appData.lightLogo} alt="logo" />
            )}
          </a>
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          onClick={handleMobileDropdown}
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="icon-bar">
            <i className="fas fa-bars"></i>
          </span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
          
            <li className="nav-item">
              <Link href={`/PageAnnonceurs`}>
                <a className="nav-link">Annonceurs</a>
              </Link>
            </li>

            <li className="nav-item">
              <Link href={`/PageInfluenceurs`}>
                <a className="nav-link">Influenceurs</a>
              </Link>
            </li>



            <li className="nav-item">
              <Link href={`/evenements`}>
                <a className="nav-link">Événements</a>
              </Link>
            </li>
           
            <li className="nav-item">
              <Link href={`/homepage/home7-dark`}>
                <a className="nav-link">Profil influenceur</a>
              </Link>
            </li>
            
            <li className="nav-item">
  <Link href="/blog-list/blog-list-dark/">
    <a target="_blank" className="nav-link">Connexion</a>
  </Link>
</li>
            
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
