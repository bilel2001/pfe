/* eslint-disable @next/next/no-img-element */
import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class Avis extends React.Component {
  constructor(props) {
    super(props);
  }
  renderArrows = () => {
    return (
      <div className="arrows">
        <div className="container">
          <div
            onClick={() => this.slider.slickNext()}
            className="next cursor-pointer"
          >
            <span className="pe-7s-angle-right"></span>
          </div>
          <div
            onClick={() => this.slider.slickPrev()}
            className="prev cursor-pointer"
          >
            <span className="pe-7s-angle-left"></span>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <section
        className={`testimonials ${
          this.props.withIMG
            ? "section-padding bg-img"
            : this.props.withCOLOR
            ? "section-padding back-color"
            : this.props.noPadding ? ""
            : "section-padding"
        } ${this.props.classText ? this.props.classText : ""}`}
        style={{
          backgroundImage: `${
            this.props.withIMG ? "url(/img/testim-img.png)" : "none"
          }`,
        }}
      >
        
             <div className="row justify-content-center">
            <div className="col-lg-8 col-md-10">
              <div className="sec-head  text-center">
                <h6 className="wow fadeIn" data-wow-delay=".5s">
                  Avis des annonceurs
                </h6>
                <h3 className="wow color-font">
                Les annonceurs  <br /> nous aiment.
                </h3>
              </div>
            </div>
          </div>
        <div className="container-fluid position-re">
          <div className="row wow fadeInUp" data-wow-delay=".5s">
            <div className="col-lg-12">
              <Slider
                className="slic-item"
                {...{
                  ref: (c) => (this.slider = c),
                  dots: false,
                  infinite: true,
                  arrows: true,
                  centerMode: true,
                  autoplay: true,
                  rows: 1,
                  slidesToScroll: 1,
                  slidesToShow: 3,
                  responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                    {
                      breakpoint: 767,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                  ],
                }}
              > 
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/emna.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Emna chouikhi
                        </h6>
                        <span className="author-details">
                          CEO 
                          One Kampio
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  Trouver Vie a été l'un des plus grands outils de développement de notre entreprise. Nous avons pu prendre ce seul canal de vente du marketing d'influence et en faire plusieurs canaux de vente.
                  </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/bilel.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Bilel Elloumi 
                        </h6>
                        <span className="author-details">
                        CEO 
                          Digix
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  Vie intègre tout dans une seule suite intuitive et rafraîchissante,
                   permettant à mon équipe d'accomplir beaucoup plus en une fraction du temps.
                  </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/ahmed.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Ahmed Louati
                        </h6>
                        <span className="author-details">
                        CEO 
                          Travel Agency
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  Je suis passé de 1,5 employé gérant notre programme d'influenceurs à 0,5 employé tout en triplant le nombre de partenariats.
                  </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/abir.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                         Abir Chabchoub
                        </h6>
                        <span className="author-details">
                        CEO Eventy
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  Je suis passé de 1,5 employé gérant notre programme d'influenceurs à 0,5 employé tout en triplant le nombre de partenariats.
                  </p>
                </div>
              </Slider>
            </div>
          </div>
          {this.renderArrows()}
        </div>
      </section>
    );
  }
}

export default Avis;
