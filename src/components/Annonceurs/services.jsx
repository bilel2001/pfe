import React from "react";
import Link from "next/link";

const Services = () => {
  return (
    <section className="services section-padding position-re">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-10">
            <div className="sec-head  text-center">
              
              <h3 className="wow color-font" data-wow-delay=".5s">
              Pourquoi les marques choisissent InfluenX</h3>
              <p className="wow fadeIn" >
              Une fois votre campagne en ligne, plus de 175 000 créateurs et influenceurs de notre réseau peuvent postuler. Gagnez du temps en recrutant tout en vous connectant avec des influenceurs authentiques qui sont vraiment enthousiasmés par votre marque
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          
          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".3s">
            <div className="step-item xcolor">
              <span className="icon pe-7s-gleam"></span>
              <h6>Échelle Puissante
Campagnes d'influence et d'affiliation</h6>
              <p>
              Tous vos créateurs, campagnes, contenus, paiements et analyses dans un tableau de bord facile à utiliser              </p>
            </div>
          </div>
          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".6s">
            <div className="step-item xbottom">
              <span className="icon pe-7s-magic-wand"></span>
              <h6>Leader de l'industrie
Recherche d'influenceurs et informations</h6>
              <p>
              300 millions de profils sur TikTok, Instagram, Youtube, Twitter, Twitch et Pinterest avec des analyses avancées pour faciliter le recrutement              </p>
            </div>
          </div>
       
         
          
        </div>
        
      </div>
      <div className="line top left"></div>
      <div className="line bottom right"></div>
    </section>
  );
};

export default Services;
