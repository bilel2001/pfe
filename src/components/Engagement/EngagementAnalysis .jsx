import { Bar } from 'react-chartjs-2';
import 'chart.js/auto';

const data = {
  labels: ['Post 1', 'Post 2', 'Post 3', 'Post 4', 'Post 5'],
  datasets: [
    {
      label: 'Engagement Rate',
      data: [2.5, 3.2, 4.1, 2.8, 3.5],
      backgroundColor: 'rgba(54, 162, 235, 0.2)',
      borderColor: 'rgba(54, 162, 235, 1)',
      borderWidth: 1
    }
  ]
};

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};

const EngagementAnalysis = () => {
  return (
    <div style={{ width: '80%', height: '400px', marginTop: "60px" }}>
      <h2>Engagement Analysis</h2>
      <Bar data={data} options={options} />
    </div>
  );
};

export default EngagementAnalysis;
