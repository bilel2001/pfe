/* eslint-disable @next/next/no-img-element */
import React from "react";
import aboutSkillsProgress from "../../common/aboutSkillsProgress";

const AboutUs5 = () => {
  React.useEffect(() => {
    aboutSkillsProgress(
      document.querySelector(".about-cr .skills-box"),
      document.querySelectorAll(".skill-progress .progres"),
      document.querySelector(".about-cr")
    );
  }, []);
  return (
    <section className="about-cr">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-5 valign">
            <div className="cont full-width">
              <h3 className="color-font"> </h3>
              <h5 className="co-tit mb-15">
              Moyen de âge
              </h5>
              <p>
                
              </p>
              <div className="skills-box mt-40">
                <div className="skill-item">
                  <h5 className="fz-14 mb-15">Entre 18 et 25</h5>
                  <div className="skill-progress">
                    <div className="progres" data-value="25%"></div>
                  </div>
                </div>
                <div className="skill-item">
                  <h5 className="fz-14 mb-15">Entre 25 et 30</h5>
                  <div className="skill-progress">
                    <div className="progres" data-value="30%"></div>
                  </div>
                </div>
                <div className="skill-item">
                  <h5 className="fz-14 mb-15">Entre 30 et 40</h5>
                  <div className="skill-progress">
                    <div className="progres" data-value="25%"></div>
                  </div>
                </div>
                <div className="skill-item">
                  <h5 className="fz-14 mb-15">+40</h5>
                  <div className="skill-progress">
                    <div className="progres" data-value="20%"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutUs5;
