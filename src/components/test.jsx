import Facebook from 'facebook-sdk';

const facebook = new Facebook({
  appId: 'votre_app_id',
  version: 'votre_version_api_facebook',
  accessToken: 'votre_jetoon_d_acces',
});

async function getPageData(pageId, fields) {
  try {
    const response = await facebook.api(`/${pageId}`, { fields });
    return response;
  } catch (error) {
    console.error(error);
    return null;
  }
}

// Fonction qui récupère l'ID d'une page Facebook à partir de son URL
function getPageIdFromUrl(url) {
  const match = url.match(/facebook\.com\/([\w.]+)/);
  if (match) {
    const pageName = match[1].split('/')[0];
    return getPageData(pageName, 'id').then(response => response.id);
  } else {
    return null;
  }
}

// Fonction qui récupère les données de la page Facebook dont l'URL est saisie par l'utilisateur
async function getPageDataFromUrl(url) {
  const pageId = await getPageIdFromUrl(url);
  if (pageId) {
    const pageData = await getPageData(pageId, 'fan_count');
    console.log(`Nombre de fans : ${pageData.fan_count}`);
  } else {
    console.log('URL de page Facebook invalide');
  }
}

// Saisie de l'URL de la page Facebook par l'utilisateur
const pageUrl = prompt('Entrez l\'URL de la page Facebook :');

// Appel de la fonction pour récupérer les données de la page Facebook saisie
getPageDataFromUrl(pageUrl);