/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import Typewriter from "typewriter-effect";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell } from "@fortawesome/free-solid-svg-icons";

import { useRouter } from 'next/router'
import api from "../../utils/axiosInstance";

const FreelancreIntro = ({ profilId }) => {
  const [image, setImage] = useState("");
  const [profil, setProfil] = React.useState(null);
  console.log(profilId)
  const [event, setEvent] = useState(null);
  React.useEffect(() => {
    const fetchData = async () => {
      try {

        const res = await api.get(`users/${profilId}`);
        setProfil(res.data)
        console.log(res.data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, [profilId]);

  const handleUpdateUser = async () => {
    try {
      const res = await api.put(`/users/${profilId}`);
      console.log(profilId);
      setEvent(res.data);
      console.log(res.data);
      console.log(profilId);
    } catch (error) {
      console.log(error);
    }
  };







  const handleImageUpload = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      setImage(e.target.result);
    };
  };

  return (
    <header className="freelancre valign">
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <div className="img">
              <img src={image || "/img/hero.jpg"} alt="" />

            </div>
          </div>
          <div className="col-lg-8 valign">
            <div className="cont">

              <h1 className="cd-headline clip">
                Hello, My name is {profil?.name} {profil?.lastname}
                <span
                  style={{ fontSize: "35px", lineHeight: "49px" }}
                  className="cd-words-wrapper"
                >
                  {/* <Typewriter
                    options={{
                      wrapperClassName: "color-font fw-600",
                      strings: [
                        "Mobile Apps",
                        "Landing Pages",
                        "Awesome Design",
                      ],
                      autoStart: true,
                      loop: true,
                    }}
                    loop={true}
                    onInit={(typewriter) => {
                      typewriter;
                    }}
                  /> */}
                </span>
              </h1>
              <div className="mt-3">
                <a href="#" className="badge badge-light mr-1">{profil?.domain}</a>  </div>
              <div className="border-top user-social-box">
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 instagram-color"> <i className="fab fa-instagram" /></span><span style={{ marginRight: '20px' }}>----</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2  facebook-color"> <i className="fab fa-facebook-square " /></span><span style={{ marginRight: '20px' }}>92,920</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 youtube-color"> <i className="fab fa-youtube" /></span><span style={{ marginRight: '20px' }}>----</span></div>

                <div className="container">
                  <div className="col-lg-8 offset-lg-9">
                    <button onClick={handleUpdateUser} className="butn light bord curve hover mt-30 mb-30">Envoyer invitation
                    </button>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div className="states">
          <div className="container">
            <ul className="flex">
              <li className="flex">
                <div className="numb valign">
                  <h3>9</h3>
                </div>
                <div className="text valign">
                  <p>
                    nomber de    <br />compagnes
                  </p>
                </div>
              </li>

              <li className="flex">
                <div className="numb valign">
                  <h3>35</h3>
                </div>
                <div className="text valign">
                  <p>
                    nomber de <br /> collaborations
                  </p>
                </div>
              </li>
              <li className="flex">
                <div className="numb valign">
                  <h3>8</h3>
                </div>
                <div className="text valign">
                  <p>
                    nomber de <br /> d'événements
                  </p>
                </div>
              </li>


              <li className="mail-us">
                <a href="mailto:your@email.com?subject=Subject">
                  <div className="flex">
                    <div className="text valign">
                      <div className="full-width">
                        <p>Entrer en contact</p>
                        <h6>Vie_Support@Gmail.Com</h6>
                      </div>
                    </div>

                    <div className="mail-icon">
                      <div className="icon-box">
                        <span className="icon color-font pe-7s-mail"></span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="line bottom left"></div>
    </header>
  );
};

export default FreelancreIntro;
