/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect } from "react";
import Split from "../Split";
import Link from "next/link";
import { thumparallaxDown } from "../../common/thumparallax";
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router'
const Campdetails = ({ projectData }) => {

  const [campagne, setCampagne] = useState(null);

  const router = useRouter()

  const { campagneId } = router.query
  const handleUpdateCampagne = async () => {
    try {
      const res = await api.put(`/campagnes/${campagneId}/part`); 
      setCampagne(res.data);
      console.log(res.data);
      console.log(campagneId);
    } catch (error) {
      console.log(error);
    }
  };



  console.log(projectData)
  React.useEffect(() => {
    setTimeout(() => {
      thumparallaxDown();
    }, 1000);
  }, []);
  return (
    <section className="min-area">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="img">
              <img
                className="thumparallax-down"
                src={`http://localhost:3001/uploads/${projectData?.photo}`} 
                alt="Uploaded Photo" 
              />
            </div>
          </div>
          <div className="col-lg-6 valign">
            <div className="content">
              <h4 className="color-font">{projectData?.namecamp}</h4>
              <Split>
                <p className="wow txt words chars splitting" data-splitting>
                {projectData?.descriptioncamp}
                </p>
                <button  onClick={handleUpdateCampagne}  className="butn light bord curve hover mt-30 mb-30">Postuler maintenant
        </button>
              </Split>
              <br />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Campdetails;
