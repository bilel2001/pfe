/* eslint-disable @next/next/no-img-element */
import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class Avis extends React.Component {
  constructor(props) {
    super(props);
  }
  renderArrows = () => {
    return (
      <div className="arrows">
        <div className="container">
          <div
            onClick={() => this.slider.slickNext()}
            className="next cursor-pointer"
          >
            <span className="pe-7s-angle-right"></span>
          </div>
          <div
            onClick={() => this.slider.slickPrev()}
            className="prev cursor-pointer"
          >
            <span className="pe-7s-angle-left"></span>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <section
        className={`testimonials ${this.props.withIMG
            ? "section-padding bg-img"
            : this.props.withCOLOR
              ? "section-padding back-color"
              : this.props.noPadding ? ""
                : "section-padding"
          } ${this.props.classText ? this.props.classText : ""}`}
        style={{
          backgroundImage: `${this.props.withIMG ? "url(/img/testim-img.png)" : "none"
            }`,
        }}
      >

        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-10">
            <div className="sec-head  text-center">
              <h6 className="wow fadeIn" data-wow-delay=".5s">
                Avis des influenceurs
              </h6>
              <h3 className="wow color-font">
                Plus de 150 000
                créateurs heureux  <br /> nous aiment.
              </h3>
            </div>
          </div>
        </div>
        <div className="container-fluid position-re">
          <div className="row wow fadeInUp" data-wow-delay=".5s">
            <div className="col-lg-12">
              <Slider
                className="slic-item"
                {...{
                  ref: (c) => (this.slider = c),
                  dots: false,
                  infinite: true,
                  arrows: true,
                  centerMode: true,
                  autoplay: true,
                  rows: 1,
                  slidesToScroll: 1,
                  slidesToShow: 3,
                  responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                    {
                      breakpoint: 767,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1,
                        centerMode: false,
                      },
                    },
                  ],
                }}
              >
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/emna.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Emna chouikhi
                        </h6>
                        <span className="author-details">
                          CEO
                          One Kampio
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  J'adore travailler avec InfluenX !Les campagnes ne cessent de s'améliorer de plus en plus !                 
                   </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/bilel.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Bilel Elloumi
                        </h6>
                        <span className="author-details">
                          CEO
                          Digix
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  InfluenX est formidable car je peux me connecter avec des dizaines de marques qui me conviennent et laisser mes statistiques parler.J'ai pu travailler sur des campagnes impressionnantes, être payé pour mon contenu et interagir avec mon public de nouvelles façons.

                  </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/ahmed.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Ahmed Louati
                        </h6>
                        <span className="author-details">
                          CEO
                          Travel Agency
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  J'ai reçu des produits et des expériences incroyables grâce à InfluenX.Les produits présentés sont tous amusants, intéressants et très utiles. Je m'assure toujours de consulter quotidiennement InfluenX afin de ne jamais manquer une opportunité !

                  </p>
                </div>
                <div className="item">
                  <div className="info valign">
                    <div className="cont">
                      <div className="author">
                        <div className="img">
                          <img src="/img/clients/abir.jpg" alt="" />
                        </div>
                        <h6 className="author-name color-font">
                          Abir Chabchoub
                        </h6>
                        <span className="author-details">
                          CEO Eventy
                        </span>
                      </div>
                    </div>
                  </div>
                  <p>
                  InfluenX a été un outil formidable pour moi en tant que créateur.J'ai eu tellement de collaborations impressionnantes grâce à eux, et j'attends avec impatience la poursuite de notre partenariat.
                  </p>
                </div>
              </Slider>
            </div>
          </div>
          {this.renderArrows()}
        </div>
      </section>
    );
  }
}

export default Avis;
