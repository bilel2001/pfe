/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Link } from "react-scroll";

const IntroInf = () => {
  return (
    <header className="slider-stwo valign position-re">
      <div className="container">
        <div className="row">
          <div className="col-lg-5">
            <div className="img">
              <img src="/img/slid/22.png" alt="" />
            </div>
          </div>
          <div className="col-lg-7 valign">
            <div className="cont">
              <div className="sub-title mb-5">
                <h6>Influenceurs</h6>
              </div>
              <h1 className="mb-10 fw-600">La plateforme leader pour les influenceurs et créateurs dans le monde.</h1>
              <p>
              Collaborez avec des marques incroyables,
                <br />développez vos canaux et gagnez plus d'argent.
              </p>
              {/* <ul>
                <li>
                  <div>
                    <span className="icon pe-7s-arc">
                      <span className="bord"></span>
                    </span>
                  </div>
                  <div className="cont">
                    <h6>Branding</h6>
                    <p>
                      It is a long established fact that a reader will be
                      distracted.
                    </p>
                  </div>
                </li>
                <li>
                  <div>
                    <span className="icon pe-7s-help2">
                      <span className="bord"></span>
                    </span>
                  </div>
                  <div className="cont">
                    <h6>Marketing</h6>
                    <p>
                      It is a long established fact that a reader will be
                      distracted.
                    </p>
                  </div>
                </li>
              </ul> */}
              <div className="col-lg-8 offset-lg-9">
                <Link href="/blog-list/blog-list-dark">
                  <a className=" butn bord curve mt-30 ">
                    <span className="color-font">Commencer</span>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default IntroInf;
