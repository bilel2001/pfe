import React from "react";
import Link from "next/link";

const Services = () => {
  return (
    <section className="services section-padding position-re">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 col-md-10">
            <div className="sec-head  text-center">

              <h3 className="wow color-font" data-wow-delay=".5s">
                Pourquoi rejoindre la
                communauté des créateurs </h3>

            </div>
          </div>
        </div>


        <div className="row">

          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".6s">
            <div className="step-item xcolor">
              <span className="icon pe-7s-global"></span>
              <h6>Connecter</h6>
              <p>
              Rejoignez des créateurs et des experts de l'industrie partageant les mêmes idées qui souhaitent vous aider à vous faire remarquer et à être payé pour la création de contenu époustouflant</p>
            </div>
          </div>
          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".9s">
            <div className="step-item xbottom">
              <span className="icon pe-7s-up-arrow"></span>
              <h6>Grandir</h6>
              <p>
              Accédez à des événements d'influence exclusifs et à des réductions sur les principales plateformes de l'économie des créateurs pour vous aider à développer votre marque personnelle</p>
            </div>
          </div>
          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".15s">
            <div className="step-item xcolor">
              <span className="icon pe-7s-users"></span>
              <h6>Collaborer</h6>
              <p>
              Trouvez des centaines de campagnes d'influence et de programmes d'affiliation de marques challenger et de leaders du marché qui souhaitent travailler avec vous</p>
            </div>
          </div>
          <div className="col-lg-6 wow fadeInUp" data-wow-delay=".12s">
            <div className="step-item xbottom">
              <span className="icon pe-7s-cash"></span>
              <h6>Gagne plus</h6>
              <p>
              Soyez payé par les marques tout en gagnant des jetons d'influence que vous pouvez échanger contre des trucs plus cool</p>
            </div>
          </div>
          {/* <div className="col-lg-6 wow fadeInUp" data-wow-delay=".6s">
            <div className="step-item xbottom">
              <span className="icon pe-7s-magic-wand"></span>
              <h6>Leader de l'industrie
Recherche d'influenceurs et informations</h6>
              <p>
              300 millions de profils sur TikTok, Instagram, Youtube, Twitter, Twitch et Pinterest avec des analyses avancées pour faciliter le recrutement              </p>
            </div>
          </div> */}



        </div>

      </div>
      <div className="line top left"></div>
      <div className="line bottom right"></div>
    </section>
  );
};

export default Services;
