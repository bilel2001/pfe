import React, { useState } from "react";

const ShopStore = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const items = [
    {
      title: "Robe",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      category: "Apps",
      price: "$1253",
    }, {
      title: "Produit ",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      category: "Apps",
      price: "$1253",
    },
    {
      title: "Hair color campaign",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      category: "Apps",
      price: "$1253",
    },{
      title: "Organic skincare",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      category: "Apps",
      price: "$1253",
    },{
      title: "Fashion Campaign",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      category: "Apps",
      price: "$1253",
    }
    // Add more items here
  ];

  return (
    <div className="store">
      <div className="top-area">
        <div className="row">
          <div className="col-lg-4 valign">
            <div className="result-text">
              <span>Showing 1 - 12 of 30 Results</span>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="form-group">
              <input
                type="text"
                placeholder="Chercher campagnes..."
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
              />
            </div>
          </div>
          <div className="col-lg-4 d-flex justify-content-end">
            <div className="filter-select">
              <select
                className="form-select"
                aria-label="Default select example"
              >
                <option defaultValue>Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        {items
          .filter((item) =>
            item.title.toLowerCase().includes(searchQuery.toLowerCase())
          )
          .map((item, index) => (
            <div className="col-lg-4 col-md-6" key={index}>
              <div className="item">
                <div className="img">
                  <img src={item.imageSrc} alt="" />
                  <span className="tag">{item.category}</span>
                  <div className="add">
                    <a href="#0">
                      Add To Cart <span className="pe-7s-angle-right"></span>
                    </a>
                  </div>
                </div>
                <div className="info">
                  <h6>{item.title}</h6>
                  <span>{item.price}</span>
                </div>
              </div>
            </div>
          ))}
      </div>
    </div>
    
  );
};

export default ShopStore;