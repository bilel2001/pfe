import React from 'react';
import { Bar } from 'react-chartjs-2';
import 'chart.js/auto';

const data = {
  labels: ['18-24', '25-34', '35-44', '45-54', '55+'],
  datasets: [
    {
      label: 'Moyenne d\'âge',
      data: [23, 31, 40, 48, 57],
      backgroundColor: 'rgba(54, 162, 235, 0.2)',
      borderColor: 'rgba(54, 162, 235, 1)',
      borderWidth: 1,
    },
  ],
};

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

const AgeAnalysis = () => {
  return (
    <div style={{ width: '80%', height: '400px', marginTop: "60px"  }}>
      <h2>Moyenne d'âge</h2>
      <Bar data={data} options={options} />
    </div>
  );
};

export default AgeAnalysis;
