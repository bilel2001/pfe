import React from "react";
import Link from "next/link";
import cardMouseEffect from "../../common/cardMouseEffect";

const Services3 = () => {
  React.useEffect(() => {
    cardMouseEffect(document.querySelectorAll(".feat .items"));
  }, []);
  return (
    <section className="feat sub-bg section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-10">
            <div className="sec-head">
              <h6 className="wow fadeIn" data-wow-delay=".5s">
                 Services
              </h6>
              <h3 className="wow color-font">
              Le #1 Gestion des influencers Plateforme 
              </h3>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3 col-md-6 items md-mb30">
            <div className="item wow fadeIn" data-wow-delay=".3s">
              <span className="icon">
                <i className="ion-ios-monitor"></i>
              </span>
              <h5>Gestion de la relation</h5>
              <p>
              De la recherche de candidats à la communication avec vos influenceurs, GRIN fournit tous les outils dont vous avez besoin pour entretenir des relations de marque authentiques à chaque étape de votre campagne de marketing d'influence.
              </p>
              <Link href="/about/about-dark">
                <a className="more-stroke">
                  <span></span>
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 items active md-mb30">
            <div className="item wow fadeIn" data-wow-delay=".3s">
              <span className="icon">
                <i className="ion-ios-bolt-outline"></i>
              </span>
              <h5>Découvrir & Recruter</h5>
              <p>
              Trouvez les influenceurs qui voient votre marque comme plus qu'un salaire. Vie vous offre plusieurs façons d'identifier et d'inviter uniquement les ambassadeurs parfaits pour votre marque.



              </p>
              <Link href="/about/about-dark">
                <a className="more-stroke">
                  <span></span>
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 items sm-mb30">
            <div className="item wow fadeIn" data-wow-delay=".3s">
              <span className="icon">
                <i className="ion-cube"></i>
              </span>
              <h5>Rapports et analyses</h5>
              <p>
              Vous aimez les relations avec votre marque, mais vous aimez aussi les résultats. 
              Suivez tous vos KPI, créez des rapports personnalisés et démontrez le retour sur 
              investissement sans une seule feuille de calcul.


              </p>
              <Link href="/about/about-dark">
                <a className="more-stroke">
                  <span></span>
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 items">
            <div className="item wow fadeIn" data-wow-delay=".3s">
              <span className="icon">
                <i className="ion-ios-color-wand"></i>
              </span>
              <h5>Gestion de contenu</h5>
              <p>
              Tirez le meilleur parti de chaque photo et publication. 
              Vie vous aide à localiser, suivre et réutiliser chaque bit d'amour que vos ambassadeurs
               de marque et créateurs de contenu diffusent sur les réseaux sociaux.


              </p>
              <Link href="/about/about-dark">
                <a className="more-stroke">
                  <span></span>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Services3;
