/* eslint-disable @next/next/no-img-element */
import React from "react";
import cardMouseEffect from "../../common/cardMouseEffect";
import { thumparallaxDown } from "../../common/thumparallax";

const MinimalArea = () => {
  React.useEffect(() => {
    cardMouseEffect(document.querySelectorAll(".feat .items"));
    setTimeout(() => {
      thumparallaxDown();
    }, 1000);
  }, []);
  return (
    <section className="min-area sub-bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="img">
              <img
                className="thumparallax-down"
                src="/img/min-area.jpg"
                alt=""
              />
            </div>
          </div>
          <div className="col-lg-6 valign">
            <div className="content pt-0">
              <h4 className="wow color-font">À propos de nous.</h4>
              <p className="wow txt" data-splitting>
              L'objectif de cette plateforme est de faciliter la collaboration entre les annonceurs et les influenceurs en leur offrant un environnement de travail intuitif et efficace. La plateforme permettra aux annonceurs de trouver des influenceurs pertinents pour leur marque et de gérer les campagnes publicitaires en temps réel.
              </p>
              <ul className="feat">
                <li className="wow fadeInUp" data-wow-delay=".2s">
                  <h6>
                    <span>1</span> Notre mission
                  </h6>
                  <p>
                  Les influenceurs auront accès à des offres d'annonceurs de qualité, pourront gérer leur profil et suivre leur performance sur la plateforme.
                  </p>
                </li>
                <li className="wow fadeInUp" data-wow-delay=".4s">
                  <h6>
                    <span>2</span> Nos buts
                  </h6>
                  <p>
                  Nous voulons créer une communauté de professionnels du marketing d'influence pour permettre aux annonceurs de trouver rapidement des influenceurs pertinents pour leurs campagnes publicitaires et permettre aux influenceurs de trouver des opportunités de collaboration.
                  </p>
                </li>
                <li className="wow fadeInUp" data-wow-delay=".6s">
                  <h6>
                    <span>3</span>Pourquoi nous?
                  </h6>
                  <p>
                  De plus, notre plateforme offre une gamme d'outils et de fonctionnalités pour simplifier le processus de gestion des campagnes d'influenceurs, de la recherche et sélection des influenceurs à la mesure des résultats de la campagne.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MinimalArea;
