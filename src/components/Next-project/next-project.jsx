import React from "react";

const NextProject = (nextProject) => {
  console.log(nextProject)
  if (!nextProject) {
    // Handle the case when projectHeaderData is null
    return <div>Loading...</div>; // or any other placeholder or loading indicator
  }
  return (
    <section className="next-prog section-padding">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="box">
              <div
                className="bg-img valign"
                style={{ backgroundImage: `url(${nextProject.photoevent})` }}

                data-overlay-dark="4"
              >
                <div className="caption ontop valign">
                  <div className="o-hidden full-width">
                    <h1>
                      <a href="">
                        <div className="stroke">Soyez Les Bienvenues.</div>
                      </a>
                    </h1>
                  </div>
                </div>
                <div className="copy-cap valign">
                  <div className="cap full-width">
                    <h1>
                      <a href="">
                        <span>Soyez Les Bienvenues.</span>
                      </a>
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NextProject;
