import React from "react";
import ContactFormData from "../../data/sections/form-info.json";
import { Formik, Form, Field } from "formik";
import Link from "next/link";

const Signannonceur = () => {
  const messageRef = React.useRef(null);
  const sendMessage = (ms) => new Promise((r) => setTimeout(r, ms));

  return (
    <section className="contact section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">
                Création de profil annonceur
              </h4>
              <Formik
                initialValues={{
                  nom: "",
                  prenom: "",
                  nom_de_societe: "",
                  telephone: "",
                  email: "",
                  instagram_url: "",
                  facebook_url: "",
                  tiktok_url: "",
                  photo: null,
                  Mot_de_passe:"",
                  
                }}
                onSubmit={async (values, { resetForm }) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));

                  // show message
                  messageRef.current.innerText =
                    "Your message has been successfully sent. I will contact you soon.";

                  // Reset the form values
                  resetForm();

                  // Clear the message after 2 seconds
                  setTimeout(() => {
                    messageRef.current.innerText = "";
                  }, 2000);
                }}
              >
                {({ errors, touched, setFieldValue }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                      <div className="form-group">
                        <Field
                          id="nom"
                          type="text"
                          name="nom"
                          placeholder="Nom"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="prenom"
                          type="text"
                          name="prenom"
                          placeholder="Prénom"
                        />
                      </div>
                      <div className="form-group">
  <Field
    id="mot_de_passe"
    type="password"
    name="mot_de_passe"
    placeholder="Mot de passe"
  />
</div>

                      <div className="form-group">
                        <Field
                          id="nom_de_societe"
                          type="text"
                          name="nom de societe"
                          placeholder="nom de societe"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="telephone"
                          type="text"
                          name="telephone"
                          placeholder="Téléphone"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="email"
                          type="email"
                          name="email"
                          placeholder="Email"
                        />
                        {errors.email && touched.email && (
                          <div>{errors.email}</div>
                        )}
                      </div>
                      <div className="form-group">
                        <Field
                          id="instagram_url"
                          type="url"
                          name="instagram_url"
                          placeholder="URL de la page Instagram"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="facebook_url"
                          type="url"
                          name="facebook_url"
                          placeholder="URL de la page Facebook"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="tiktok_url"
                          type="url"
                          name="tiktok_url"
                          placeholder="URL de la page TikTok"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="date_naissance"
                          type="date"
                          name="date_naissance"
                          placeholder="Date de naissance"
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="file"
                          onChange={(event) => {
                            setFieldValue("photo", event.currentTarget.files[0]);
                          }}
                        />
                      </div>
                    </div>
                    





                    <Link href={`/blog-list/blog-list-dark/`}>

                      <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                        <span>Enregistrer</span>
                      </button>
                    </Link>
                    <Link href={`/blog-list/blog-list-dark/`}>
                      < button type="submit" className="butn bord curve mt-30">
                        <span>Retour</span>
                      </button>
                    </Link>

                  </Form>
                )}
              </Formik>
            </div>
          </div>

        </div>
      </div>

    </section>
  );
};

export default Signannonceur;
