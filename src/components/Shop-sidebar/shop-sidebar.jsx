import React, { useState } from "react";

const ShopSidebar = () => {
  
  const tooltipRef = React.useRef(),
  setValue = (range) => {
    const newValue = Number(
        ((range.value - range.min) * 100) / (range.max - range.min)
      ),
      newPosition = 16 - newValue * 0.32;
    tooltipRef.current.innerHTML = `<span>${range.value}</span>`;
    tooltipRef.current.style.left = `calc(${newValue}% + (${newPosition}px))`;
    document.documentElement.style.setProperty(
      "--range-progress",
      `calc(${newValue}% + (${newPosition}px))`
    );
    let a = range.value;
    range.value = a;
  };

const [searchValue, setSearchValue] = useState(""); // Ajout de l'état pour la valeur de recherche

const handleSearchChange = (event) => {
  setSearchValue(event.target.value); // Mettre à jour la valeur de recherche lorsque l'input change
};

React.useEffect(() => {
  setValue(document.querySelector("#range"));
}, []);

  return (
    <div className="sidebar  md-mb50">
      <div className="row">
        <div className="col-lg-12 col-md-6">
          <div className="search mb-30">
            <form action="">
              <div className="form-group">
                <input 
                type="text" 
                name="shop-search"
                 placeholder="Search"
                 value={searchValue} // Utilisation de la valeur de recherche dans l'input
                 onChange={handleSearchChange} // Gestionnaire d'événements pour mettre à jour la valeur de recherche
               />
                <button>
                  <span className="icon pe-7s-search"></span>
                </button>
              </div>
            </form>
          </div>
        </div>

        <div className="col-lg-12 col-md-6">
          <div className="box gat mb-30">
            <ul>
              <li>
                <h6 href="#0">
                 Domaines 
                </h6>
              </li>
              <li>
                <a href="#0">
                  Digital <span>03</span>
                </a>
              </li>
              <li>
                <a href="#0">
                  Lifestyle <span>07</span>
                </a>
              </li>
              <li>
                <a href="#0">
                  Sport <span>04</span>
                </a>
              </li>
              <li>
                <a href="#0">
                  Lifestyle <span>09</span>
                </a>
              </li>
              <li>
                <a href="#0">
                  Fashion <span>14</span>
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="col-lg-12 col-md-6">
          <div className="box filter-price mb-30">
            <h6 className="title mb-30">Nombre d'abonnés</h6>

            <div className="range-slider mb-30">
              <div id="tooltip" ref={tooltipRef}></div>
              <input
                onInput={(e) => setValue(e.currentTarget)}
                id="range"
                type="range"
                step="10"
                min="10"
                max="1000"
              />
              <div className="start-pointe">10 K</div>
            </div>
          </div>
        </div>

        <div className="col-lg-12 col-md-6">
          <div className="box tags">
            <h6 className="title mb-30">Réseaux sociaux</h6>

            <div>
              <a href="#0">Instagram</a>
              <a href="#0">Facebook</a>
              <a href="#0">YouTube</a>
              <a href="#0">TikTok</a>
            </div>
            
          </div>
        </div>
        <div className="col-lg-12 col-md-6 mt-4">
          <div className="box tags">
            <h6 className="title mb-30">Pays</h6>

            <div className="filter-select">
              <select
                className="form-select"
                aria-label="Default select example"
              >
                <option defaultValue>Séléctionner une pays</option>
                <option value="1">France</option>
                <option value="2">Tunisie</option>
                <option value="3">US</option>
              </select>
            </div>
            
          </div>
        </div>

        
      </div>
    </div>
  );
};

export default ShopSidebar;
