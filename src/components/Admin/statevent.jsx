import React, { useEffect } from 'react';
import Chart from 'chart.js/auto';



const Statevent = () => {
    useEffect(() => {

        // Chart Global Color
        Chart.defaults.color = "#6C7293";
        Chart.defaults.borderColor = "#000000";
        
        
        var ctx4 = document.getElementById("bar-chart").getContext("2d");
        var myChart4 = new Chart(ctx4, {
            type: "bar",
            data: {
                labels: ["sport", "food", "fashion", "lifestyle"],
                datasets: [{
                    
                    label: "2023",
                    backgroundColor: "rgba(52, 152, 219, .7)",
                    data: [12, 17, 22, 9]
                }]
            },
            options: {
                responsive: true
            }
        });

    }, []);

    return (
        <div>
            <div className="container-fluid pt-4 px-4" style={{
                width: '2500px',
                paddingRight: '1000px',
                paddingLeft: '10px',
                marginTop: '100px',
                marginRight: '00px',
                marginLeft: '250px',
            }}>
                <div className="row g-4">
                
                    <div className="col-sm-12 col-xl-6 ">
                        <div className="bg1 rounded h-100 p-4">
                            <h3 className="mb-4 color-font">Nombre d'évènements par domaines</h3>
                            <canvas id="bar-chart" />
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>
    );
};

export default Statevent;
