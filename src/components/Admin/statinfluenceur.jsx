import React, { useEffect } from 'react';
import Chart from 'chart.js/auto';

import Tableauinfluenceur from './Tableauinfluenceur';

const StatInfluenceur = () => {
    useEffect(() => {
        // Chart Global Color
        Chart.defaults.color = "#6C7293";
        Chart.defaults.borderColor = "#000000";

        
        // Pie Chart
        var ctx6 = document.getElementById("doughnut-chart");
        if (ctx6) {
            var myChart6 = new Chart(ctx6, {
                type: "doughnut",
                data: {
                    labels: ["Hommes", "Femmes"],
                    datasets: [{
                        backgroundColor: [
                            "#2fb5ea",
                            "#eb16c8"
                        ],
                        data: [40, 60]
                    }]
                },
                options: {
                    responsive: true
                }
            });
        }
        var ctx4 = document.getElementById("bar-chart").getContext("2d");
        var myChart4 = new Chart(ctx4, {
            type: "bar",
            data: {
                labels: ["sport", "food", "fashion", "lifestyle"],
                datasets: [{
                    label: "Homme",
                    backgroundColor: "#2fb5ea",
                    data: [75, 30, 30, 40]
                },
                {
                    label: "Femme",
                    backgroundColor: "#eb16c8",
                    data: [60, 80, 40, 50]
                }]
            },
            options: {
                responsive: true
            }
        });

    }, []);

    return (
        <div>
            <div className="container-fluid pt-4 px-4" style={{
                width: '1200px',
                paddingRight: '100px',
                paddingLeft: '10px',
                marginTop: '100px',
                marginRight: '100px',
                marginLeft: '290px',
            }}>
                <div className="row g-4">
                <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h5 className="mb-4 color-font">Répartition par genre </h5>
                            <canvas id="doughnut-chart" />
                        </div>
                    </div>
                    <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h5 className="mb-4 color-font">Nombre d'influenceurs par domaines</h5>
                            <canvas id="bar-chart" />
                        </div>
                    </div>
                   
                </div>
            </div>
            <Tableauinfluenceur/>
        </div>
    );
};

export default StatInfluenceur;
