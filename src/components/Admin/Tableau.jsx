import React from 'react'

const Tableau = () => {
  return (
    <div>
      
{/* Recent Sales Start */}
<div className="container-fluid pt-4 px-4"
style={{
  width: '1120px',
  paddingRight: '100px',
  paddingLeft: '10px',
  marginTop: '10px',
  marginRight: '100px',
  marginLeft: '320px',
  marginBottom:'30px'
}}>
  <div className="bg1 text-center rounded p-4">
    <div className="d-flex align-items-center justify-content-between mb-4">
      <h3 className="mb-0 color-font">Tableau d'utilisateurs</h3>
     
    </div>
    <div className="table-responsive">
      <table className="table text-start align-middle table-bordered table-hover mb-0">
      <thead>
  <tr className="color-font text-center">
    
    <th scope="col">Id</th>
    <th scope="col">Nom d'utilisateur</th>
    <th scope="col">Type</th>
  
    
    <th scope="col">Action</th>
  </tr>
</thead>

        <tbody>
          <tr className='text-center'>
            
            <td>01</td>
            <td>bile elloumi</td>
            <td>influenceur</td>
            
            
            <td><a className="btn btn-sm btn-primary" href="#">Consulter</a></td>
          </tr>
          <tr className='text-center'>
           
            <td>02</td>
            <td>emna chouikhi</td>
            <td>influenceur</td>
           
            
            <td><a className="btn btn-sm btn-primary" href="#">Consulter</a></td>
          </tr>
          <tr className='text-center'>
          
            <td>03</td>
            <td>abir chabchoub </td>
            <td>annonceur</td>
            
        
            <td><a className="btn btn-sm btn-primary" href="#">Consulter</a></td>
          </tr>
          <tr className='text-center'>
           
            <td>04</td>
            <td>ahmed louati</td>
            <td>influenceur</td>
           
            
            <td><a className="btn btn-sm btn-primary" href="#">Consulter</a></td>
          </tr>
          <tr className='text-center'>
           
            <td>05</td>
            <td>imen louati</td>
            <td>annonceur</td>
           
           
            <td><a className="btn btn-sm btn-primary" href="#">Consulter</a></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
{/* Recent Sales End */}


    </div>
  )
}

export default Tableau
