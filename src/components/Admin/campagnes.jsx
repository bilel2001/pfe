import React, { useEffect, useState } from 'react';
import axios from 'axios';
import api from "../../utils/axiosInstance";

const  Campagne = () => {
  const [filteredEvents, setFilteredEvents] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const instance = axios.create({ baseURL: 'http://localhost:3001' });
        delete instance.defaults.headers.common['Authorization'];
        const domaincamp = '';
        const { data } = await instance.get('/api/campagnes/domain', { params: { domaincamp } });
        console.log(data);
        setFilteredEvents(data);
        
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);
  const handleDelete = async (e, campagneId) => {
    e.preventDefault()
    await api.delete(`campagnes/${campagneId}/delete`)
  }

  return (
    <div>
      
    {/* Recent Sales Start */}
    <div className="container-fluid pt-4 px-4"
    style={{
      width: '1200px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '100px',
      marginBottom:'30px',
      marginRight: '100px',
      marginLeft: '280px',
    }}>
      <div className="bg1 text-center rounded p-4">
        <div className="d-flex align-items-center justify-content-between mb-4">
          <h3 className="mb-0 color-font" >Campagnes</h3>

        </div>
        <div className="table-responsive">
          <table className="table text-start align-middle table-bordered table-hover mb-0">
          <thead>
      <tr className="color-font text-center">
        
        <th scope="col">Campagnes</th>
        <th scope="col">Domaine</th>
     
        <th scope="col">Action</th>
      </tr>
    </thead>
    
            <tbody className='text-center text-white'>
            {filteredEvents.map((event) => (
           <tr key={event}>
            
            
            <td>{event.namecamp}</td>
            
            <td>{event.domaincamp}</td>
            
            
            
            <td>
            <button onClick={(e) => handleDelete(e, event?._id)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>

            </td>
          </tr>
        ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {/* Recent Sales End */}
    
    
        </div>
  )
}

export default Campagne
