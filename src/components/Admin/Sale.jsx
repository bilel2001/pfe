import React from 'react'
import Head from "next/head";
const Sale = () => {
  return (
    <>
    <Head>
    <link rel="stylesheet" href="/css/ionicons.min.css" />
  </Head>
    <div>
      <div className="container-fluid pt-4 px-4"style={{
      width: '1100px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '100px',
      marginRight: '100px',
      marginLeft: '320px',
    }}
      >
  <div className="row g-4" style={{width:'1100px'}}>
    <div className="col-sm-6 col-xl-3">
      <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
        <i className="pe-7s-speaker text-primary fs-3" />
        <div className="ms-3">
          <p className="mb-2">Nombre de campagnes</p>
          <h6 className="mb-0">5</h6>
        </div>
      </div>
    </div>
    <div className="col-sm-6 col-xl-3">
      <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
        <i className="ion-android-person text-primary fs-3" />
        <div className="ms-3">
          <p className="mb-2">Nombre d'influenceurs</p>
          <h6 className="mb-0">3</h6>
        </div>
      </div>
    </div>
    <div className="col-sm-6 col-xl-3">
  <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
    <i className="ion-android-person text-primary fs-3" />
    <div className="ms-3">
      <p className="mb-2">Nombre d'annonceurs</p>
      <h6 className="mb-0">5</h6>
    </div>
  </div>
</div>

<div className="col-sm-6 col-xl-3">
  <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
    <i className="bi bi-calendar-event text-primary fs-3" />
    <div className="ms-3">
      <p className="mb-2">Nombre d'événements</p>
      <h6 className="mb-0">4</h6>
    </div>
  </div>
</div>

  </div>
</div>
    </div>
    </>
  )
}

export default Sale
