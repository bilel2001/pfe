import React, { useEffect } from "react";
import Chart from "chart.js/auto";

const SalesChart = () => {
  useEffect(() => {
    Chart.defaults.color = "#6C7293";
    Chart.defaults.borderColor = "#000000";
    const ctx1 = document.getElementById("worldwide-sales");
    const worldwideSalesChart = new Chart(ctx1, {
      type: "line",
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
        datasets: [
          {
            label: "Utilisateurs",
            data: [250, 300, 200, 350, 400, 450],
            borderColor: "#eb16c8",
            fill: false,
            tension: 0.4,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                precision: 0,
              },
            },
          ],
        },
        plugins: {
          legend: {
            display: false,
          },
        },
      },
    });

    const ctx2 = document.getElementById("sales-revenue");
    const salesRevenueChart = new Chart(ctx2, {
      type: "bar",
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
        datasets: [
          {
            label: "Influenceurs",
            data: [120, 150, 200, 250, 270, 350],
            backgroundColor: "#eb16c8",
            
          },
          {
            label: "Annonceurs",
            data: [100, 120, 130, 160, 180, 200],
            backgroundColor: "#2fb5ea",
            
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                precision: 0,
              },
            },
          ],
        },
        plugins: {
          legend: {
            display: false,
          },
        },
      },
    });


    return () => {
      worldwideSalesChart.destroy();
      salesRevenueChart.destroy();
    };
  }, []);


  return (

    <div className="container-fluid pt-4 px-4" style={{
      width: '1120px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '10px',
      marginRight: '100px',
      marginLeft: '320px',
    }}>

      <div className="row g-4" >
        <div className="col-sm-12 col-xl-6">
          <div
            className="bg1 text-center rounded p-4"
            style={{ backgroundColor: "#333", color: "#fff" }}
          >
            <div className="d-flex align-items-center justify-content-between mb-4" style={{ display: 'flex !important' }}>
              <h6 className="mb-0 color-font" style={{ marginBottom: "0!important" }}>Nombre d'utilistaeurs par mois</h6>
              
            </div>
            <canvas id="worldwide-sales" />

          </div>
        </div>
        <div className="col-sm-12 col-xl-6" style={{ flex: "0 0 auto", width: "100%" }}>
          <div
            className="bg1 text-center rounded p-4"
            style={{ backgroundColor: "#333", color: "#fff" }}
          >
            <div className="d-flex align-items-center justify-content-between mb-4" style={{ display: 'flex !important' }}>
              <h6 className="mb-0 color-font" style={{ marginBottom: "0!important" }}>Nombre d'influenceurs et anoonceurs par mois</h6>
              
            </div>
            <canvas id="sales-revenue" />
          </div>
    </div>
      </div>
    </div>
  );
};

export default SalesChart;
