import { Line } from 'react-chartjs-2';

const data = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
  datasets: [
    {
      label: 'Followers',
      data: [1000, 1200, 1500, 1800, 2000],
      fill: false,
      borderColor: 'rgb(75, 192, 192)',
      tension: 0.1
    }
  ]
};

const options = {
  scales: {
    xAxes: [
      {
        type: 'linear',
        position: 'bottom',
        ticks: {
          beginAtZero: true
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};

const GrowthChart = () => {
  return (
    <div style={{ width: '4000px', height: '400px', marginTop: "100px" , marginLeft: "500px" }}>
      <h2>Growth Chart</h2>
      <Line data={data} options={options} />
    </div>
  );
};

export default GrowthChart;
