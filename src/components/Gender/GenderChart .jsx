import { Pie } from 'react-chartjs-2';
import 'chart.js/auto';

const data = {
  labels: ['Femmes', 'Hommes'],
  datasets: [
    {
      label: 'Sexe',
      data: [60, 40,],
      backgroundColor: ['#ff6384', '#36a2eb'],
      borderColor: ['#ff6384', '#36a2eb'],
      borderWidth: 1
    }
  ]
};

const options = {
  responsive: true,
  maintainAspectRatio: false,
  title: {
    display: true,
    text: 'Répartition par sexe'
  }
};

const GenderChart = () => {
  return (
    <div style={{ width: '80%', height: '400px', marginTop: "60px"  }}>
      <h2>Informations sur les sexes</h2>
      <Pie data={data} options={options} />
    </div>
  );
};

export default GenderChart;
