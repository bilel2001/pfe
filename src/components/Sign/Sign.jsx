import React, { useState } from "react";
import { Formik, Form, Field } from "formik";
import Link from "next/link";
import axios from "axios";

const Sign = () => {
  const [message, setMessage] = useState("");
  const [error, setError] = useState("");
  const handleSubmit = async (values) => {
    console.log("values  : ",values);
    try {
      const instance = axios.create({baseURL: 'http://localhost:3001'})
      delete instance.defaults.headers.common["Authorization"];
      const response = await instance.post("/api/users", values);
      console.log(response.data);
      setMessage("Compte créé avec succès");
    } catch (error) {
      console.log(error);
      setMessage("email deja utilisé");
    }
    
  };

  return (
    <section className="contact section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">
                Création de profil 
              </h4>
              <Formik
                initialValues={{
                  name: "",
                  lastname: "",
                  password: "",
                  email: "",
                  role: "",
                }}
                onSubmit={handleSubmit}
              >
                {({ isSubmitting }) => (
                  <Form id="contact-form">
                    <div className="messages">
  {message && <p>{message}</p>}
</div>
                    <div className="controls">
                      <div className="form-group">
                        <Field
                          id="nom"
                          type="text"
                          name="name"
                          placeholder="Nom"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="prenom"
                          type="text"
                          name="lastname"
                          placeholder="Prénom"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="email"
                          type="email"
                          name="email"
                          placeholder="Email"
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="mot_de_passe"
                          type="password"
                          name="password"
                          placeholder="Mot de passe"
                        />
                      </div>
                      <div className="form-group">
                        <label>Choisir votre role:</label>
                        <div
                          style={{ display: "flex", flexDirection: "row-reverse" }}
                        >
                          <div style={{ marginLeft: "10px" }}>
                            <label>
                              <Field
                                type="radio"
                                name="role"
                                value="influenceur"
                              />
                              Influenceur
                            </label>
                          </div>
                          <div>
                            <label>
                              <Field
                                type="radio"
                                name="role"
                                value="annonceur"
                              />
                              Annonceur
                            </label>
                          </div>
                        </div>
                      </div>
                      <button
                        type="submit"
                        disabled={isSubmitting}
                        className="butn bord curve mt-30"
                        style={{ marginRight: "10px" }}
                      >
                        <span>Enregistrer</span>
                      </button>
                      <Link href={`/blog-list/blog-list-dark/`}>
                        <button
                          type="submit"
                          className="butn bord curve mt-30"
                        >
                          <span>Retour</span>
                        </button>
                      </Link>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Sign;