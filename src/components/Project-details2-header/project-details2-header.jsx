import React, { useState, useEffect } from "react";
import Link from "next/link";
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router'
const ProjectDetails2Header = ({ projectHeaderData }) => {
  const [event, setEvent] = useState(null);

  const router = useRouter()

  const { eventId } = router.query
  const handleUpdateEvent = async () => {
    try {
      const res = await api.put(`/events/${eventId}/part`); 
      setEvent(res.data);
      console.log(res.data);
      console.log(eventId);
    } catch (error) {
      console.log(error);
    }
  };
    
  

  console.log(projectHeaderData)
  if (!projectHeaderData) {
    // Handle the case when projectHeaderData is null
    return <div>Loading...</div>; // or any other placeholder or loading indicator
  }
  return (
   
    <section
  className="page-header proj-det bg-img parallaxie valign"
  style={{ backgroundImage: `url(http://localhost:3001/uploads/${projectHeaderData?.photoevent})` }}
  data-overlay-dark="4"
>
      <div className="container">
        <div className="row">
          <div className="col-lg-7 col-md-9">
            <div className="cont">
              <h6>{projectHeaderData.domainevent}</h6>
              <h2>{projectHeaderData.title}</h2>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3">
            <div className="item mt-30">
              <h6>Organisateur</h6>
              <p>
                
                  <a>{projectHeaderData.organiseurevent}</a>
                
              </p>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="item mt-30">
              <h6>Date</h6>
              <p>{projectHeaderData.dated}</p>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="item mt-30">
              <h6>Lieu</h6>
              <p>
                
                <a>{projectHeaderData.lieu}</a>
              
            </p>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="container">
        <button  onClick={handleUpdateEvent}  className="butn light bord curve hover mt-30 mb-30">Participer
        </button>
        </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProjectDetails2Header;
