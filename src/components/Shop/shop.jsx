
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import axios from 'axios';

const Shop = () => {
  const [allEvents, setAllEvents] = useState([]);
  const [filteredCampagnes, setFilteredCampagnes] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const instance = axios.create({ baseURL: 'http://localhost:3001' });
        delete instance.defaults.headers.common['Authorization'];
        const domaincamp = '';
        const { data } = await instance.get('/api/campagnes/domain', { params: { domaincamp } });
        
        setFilteredCampagnes(data);
       
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  // sidebar
  const tooltipRef = React.useRef(),
    setValue = (range) => {
      const newValue = Number(
        ((range.value - range.min) * 100) / (range.max - range.min)
      ),
        newPosition = 16 - newValue * 0.32;
      tooltipRef.current.innerHTML = `<span>${range.value}</span>`;
      tooltipRef.current.style.left = `calc(${newValue}% + (${newPosition}px))`;
      document.documentElement.style.setProperty(
        "--range-progress",
        `calc(${newValue}% + (${newPosition}px))`
      );
      let a = range.value;
      range.value = a;
    };

  const [searchValue, setSearchValue] = useState(""); // Ajout de l'état pour la valeur de recherche

  const handleSearchChange = (event) => {
    setSearchValue(event.target.value); // Mettre à jour la valeur de recherche lorsque l'input change
  };

  React.useEffect(() => {
    setValue(document.querySelector("#range"));
  }, []);

  // camp

  const [searchQuery, setSearchQuery] = useState("");
  const items = [
    {
      title: "Robe",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      domain: "Fashion",

    }, {
      title: "Produit ",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      domain: "Apps",

    },
    {
      title: "Hair color campaign",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      domain: "Coif",

    }, {
      title: "Organic skincare",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      domain: "Cosmetics",

    }, {
      title: "Fashion Campaign",
      imageSrc: "/img/mobile-app/shop/1.jpg",
      domain: "Fashion",

    }
    // Add more items here
  ];

  return (
    <section className="shop section-padding">
      <div className="container">
        <div className="row">
          {/* sidebar */}
          <div className="col-lg-3">
            <div className="sidebar  md-mb50">
              <div className="row">
                <div className="col-lg-12 col-md-6">
                  <div className="search mb-30">
                    <form action="">
                      <div className="form-group">
                        <input
                          type="text"
                          name="shop-search"
                          placeholder="Chercher campagnes..."
                          value={searchQuery}
                          onChange={(e) => setSearchQuery(e.target.value)}
                        />
                        <button>
                          <span className="icon pe-7s-search"></span>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-lg-12 col-md-6">
                  <div className="box gat mb-30">
                    <ul>
                      <li>
                        <h6 href="#0">
                          Domaines
                        </h6>
                      </li>
                      <li>
                        <a href="#0">
                          Digital <span>03</span>
                        </a>
                      </li>
                      <li>
                        <a href="#0">
                          Lifestyle <span>07</span>
                        </a>
                      </li>
                      <li>
                        <a href="#0">
                          Sport <span>04</span>
                        </a>
                      </li>
                      <li>
                        <a href="#0">
                          Lifestyle <span>09</span>
                        </a>
                      </li>
                      <li>
                        <a href="#0">
                          Fashion <span>14</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-12 col-md-6">
                  <div className="box gat mb-30">
                    <h6 className="title mb-50">Pays</h6>

                  </div>
                </div>
                <div className="col-lg-12 col-md-6 ">
                  <div className="box filter-price mb-30">
                    <h6 className="title mb-30">Nombre d'abonnés</h6>

                    <div className="range-slider mb-30">
                      <div id="tooltip" ref={tooltipRef}></div>
                      <input
                        onInput={(e) => setValue(e.currentTarget)}
                        id="range"
                        type="range"
                        step="10"
                        min="10"
                        max="1000"
                      />
                      <div className="start-pointe">10 K</div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 col-md-6">
                  <div className="box tags">
                    <h6 className="title mb-30">Réseaux sociaux</h6>

                    <div>
                      <a href="#0">Instagram</a>
                      <a href="#0">Facebook</a>
                      <a href="#0">YouTube</a>
                      <a href="#0">TikTok</a>
                    </div>

                  </div>
                </div>



              </div>
            </div>
          </div>

{/* campaign */}
          <div className="col-lg-9">
            <div className="store">
              <div className="top-area">
                <div className="row">


                  <div className="col-lg-4 valign">

                    <div className="filter-select" >
                      <select
                        className="form-select"
                        aria-label="Default select example"
                        style={{ marginLeft: '-275px', marginTop: '480px' }}
                      >
                        <option defaultValue>Choisir pays</option>
                        <option value="1">France</option>
                        <option value="2">US</option>
                        <option value="3">UK</option>
                      </select>
                    </div>


                    <div style={{
                      width: "1000px",
                      marginLeft: "650px",
                      marginRight: "400px",
                      paddingLeft: "10px",
                      paddingRight: "1000px",
                      whiteSpace: "nowrap",
                      marginTop: "-500px"
                    }}>
                      <div className="result-text">
                        <span>Showing 1 - 12 of 30 Results</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row" style={{

                marginTop: "-480px"
              }}>
                {filteredCampagnes
  .filter((campagne) =>
  campagne.namecamp.toLowerCase().includes(searchQuery.toLowerCase())
  )
  .map((campagne, index) => (
    <div className="col-lg-4 col-md-6" key={index}>
      <div className="item">
        <div className="img">
        {campagne ? (
    <img src={`http://localhost:3001/uploads/${campagne.photo}`} alt="Uploaded Photo" />
  ) : (
    <p>Loading...</p>
  )}
          <span className="tag">{campagne.domaincamp}</span>
          <div className="add">
            <Link href={`/campagnes/${campagne._id}`}>
              <a href="#0">
                Consulter <span className="pe-7s-angle-right"></span>
              </a>
            </Link>
          </div>
        </div>
        <div className="info">
          <h6>{campagne.namecamp}</h6>
          <span>{campagne.nbfollower}</span>
        </div>
      </div>
    </div>
  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Shop;
