import React from "react";

const ProjectDescription = ({ projectDescriptionData }) => {
  console.log(projectDescriptionData)
  if (!projectDescriptionData) {
    // Handle the case when projectHeaderData is null
    return <div>Loading...</div>; // or any other placeholder or loading indicator
  }
  return (
    <section className="intro-section section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-4">
            <div className="htit">
              <h4>
               Description
              </h4>
            </div>
          </div>
          <div className="col-lg-8 offset-lg-1 col-md-8">
            <div className="text js-scroll__content">
              <p className="extra-text">{projectDescriptionData.Descriptionevent}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProjectDescription;
