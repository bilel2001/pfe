import { Doughnut } from 'react-chartjs-2';
import 'chart.js/auto';

const data = {
  labels: ['Post 1', 'Post 2', 'Post 3', 'Post 4', 'Post 5'],
  datasets: [
    {
      label: 'Likes',
      data: [1000, 1200, 1500, 1800, 2000],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)'
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)'
      ],
      borderWidth: 1
    }
  ]
};

const options = {
  legend: {
    position: 'right'
  }
};

const BestPerformingPosts = () => {
  return (
    <div style={{ width: '80%', height: '400px', marginTop: "60px"  }}>
      <h2>Best Performing Posts</h2>
      <Doughnut data={data} options={options} />
    </div>
  );
};

export default BestPerformingPosts;
