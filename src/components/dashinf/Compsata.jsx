import React from 'react'
import Head from "next/head";
import { Badge, Grid } from "@nextui-org/react";
import { StyledBadge } from "../../../public/js/StyledBadge";
const  Compsata = () => {
   
    return (
        <>
            <Head>
                <link rel="stylesheet" href="/css/ionicons.min.css" />
            </Head>
            <div className='container'>
                <div className="container-fluid pt-4 px-4" style={{
                    width: '1050px',
                    paddingRight: '100px',
                    paddingLeft: '10px',
                    marginTop: '100px',
                    marginRight: '100px',
                    marginLeft: '220px',
                }}
                >
                    <div className="row g-4">
                        <div className="col-sm-6 col-xl-3">
                            <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
                                <Grid>
                                   
                                <StyledBadge type="activé">Activé</StyledBadge>
                                    
                                </Grid>
                                <div className="ms-3">
                                    <p className="mb-2">Nombre de compagnes </p>
                                    <h6 className="mb-0">15 
                                    </h6>
                                </div>
                            </div>
                        </div>


                        <div className="col-sm-6 col-xl-3">
                            <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
                                <Grid>
                                <StyledBadge type="terminé">Terminé</StyledBadge>
                                </Grid>        <div className="ms-3">
                                    <p className="mb-2">Nombre de compagnies</p>
                                    <h6 className="mb-0">60</h6>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6 col-xl-3">
                            <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
                                <Grid>
                                <StyledBadge type="encours">En cours</StyledBadge>
                                </Grid>
                                <div className="ms-3">
                                    <p className="mb-2">Nombre de compagnies</p>
                                    <h6 className="mb-0">40</h6>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-6 col-xl-3">
                            <div className="bg1 rounded d-flex align-items-center justify-content-between p-4">
                                <Grid>
                                <StyledBadge type="annulé">Annulé</StyledBadge>
                                </Grid>
                                <div className="ms-3">
                                    <p className="mb-2">Nombre de compagnies</p>
                                    <h6 className="mb-0">234</h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </>

    )
}

export default Compsata
