import React from "react";
import ContactFromDate from "../../data/sections/form-info.json";
import { Formik, Form, Field } from "formik";
import Link from "next/link";

const paramform = () => {
  const messageRef = React.useRef(null);
  const sendMessage = (ms) => new Promise((r) => setTimeout(r, ms));
  return (
    <section className="contact section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Modifier le profil.</h4>
              <Formik
                initialValues={{
                  nom: "",
                  email: "",
                  "instagram profil": "",
                  "facebook profil": "",
                  "tiktok profil": ""
                }}
                onSubmit={async (values) => {
                  await sendMessage(500);
                  alert(JSON.stringify(values, null, 2));
                  // show message

                  messageRef.current.innerText =
                    "Your Message has been successfully sent. I will contact you soon.";
                  // Reset the values
                  values.nom = "";
                  values.email = "";
                  values["instagram profil"] = "";
                  values["facebook profil"] = "";
                  values["tiktok profil"] = "";
                  // clear message
                  setTimeout(() => {
                    messageRef.current.innerText = ''
                  }, 2000)
                }}
              >
                {({ errors, touched }) => (
                  <Form id="contact-form">
                    <div className="messages" ref={messageRef}></div>
                    <div className="controls">
                      <div className="form-group">
                        <Field
                          id="form_name"
                          type="text"
                          name="nom"
                          placeholder="Nom"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                      <Field
                        id="tiktok_profil"
                        type="text"
                        name="tiktok profil"
                        placeholder="etiquet"
                      />
                    </div>
                      <div className="form-group">
                        <Field
                          id="form_email"
                          type="email"
                          name="email"
                          placeholder="Email"
                        />
                        {errors.email && touched.email && (
                          <div>{errors.email}</div>
                        )}
                      </div>
                    </div>
                    <div className="form-group">
                      <Field
                        id="instagram_profil"
                        type="text"
                        name="instagram profil"
                        placeholder="lien instagram page"
                      />
                    </div>

                    <div className="form-group">
                      <Field
                        id="facebook_profil"
                        type="text"
                        name="facebook profil"
                        placeholder="lien facebook page"
                      />
                    </div>
                    <div className="form-group">
                      <Field
                        id="tiktok_profil"
                        type="text"
                        name="tiktok profil"
                        placeholder="lien tiktok page"
                      />
                    </div>
                    <div className="form-group">
                      <Field
                        id="date_naissance"
                        type="date"
                        name="date_naissance"
                        placeholder="date denaissance"
                      />
                    </div>




                    <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                      <span>Enregistrer</span>
                    </button>
                    <Link href={`/homepage/home7-dark/`}>
                    < button type="submit" className="butn bord curve mt-30">
                      <span>Retour</span>
                    </button>
                    </Link>

                  </Form>
                )}
              </Formik>
            </div>
          </div>

        </div>
      </div>

    </section>
  );
};

export default paramform;
