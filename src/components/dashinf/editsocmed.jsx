import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, useFormikContext } from "formik";
import axios from 'axios';


const Editsocmed = () => {
  const [message, setMessage] = useState("");
  let id = '';


  if (typeof window !== 'undefined') {
    const userData = localStorage.getItem('userData');
    const parsedUserData = JSON.parse(userData);
    id = parsedUserData._id;

  }

  const handleSubmit = async (values) => {
    

    try {
      const instance = axios.create({ baseURL: 'http://localhost:3001' });
      delete instance.defaults.headers.common['Authorization'];
      const { linkinsta, linkfcb, linktik, linkyou } = values

      await instance.put("api/users/updateSoc", { id, linkinsta, linkfcb, linktik, linkyou });
      setMessage("ajoute page avec succès");

    } catch (error) {
      console.log(error);
    }
  };


 
  return (
    <section className="contact section-padding" style={{
      width: '950px',
      paddingRight: '100px',
      paddingLeft: '10px',

      marginRight: '100px',
      marginLeft: '335px',
    }} >
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Modifier le profil.</h4>
              <Formik
                initialValues={{
                  linkinsta: '',
                  linkfcb: "",
                  linktik: "",
                  linkyou: "",
                  
                }}
                onSubmit={handleSubmit}
              >
                {({ errors, touched, isSubmitting, setFieldValue }) => {
                  const [user, setUser] = useState({});
                  const [showPassword, setShowPassword] = useState(false);

                  useEffect(() => {
                    const fetchData = async () => {
                      try {
                        const instance = axios.create({ baseURL: 'http://localhost:3001' });
                        delete instance.defaults.headers.common['Authorization'];
                        const { data } = await instance.get('api/users', { params: { id } });
                        const fields = ['linkinsta', 'linkfcb', 'linktik', 'linkyou'];
                        fields.forEach(field => setFieldValue(field, data[field], false));
                      } catch (error) {
                        console.log(error);
                      }
                    };
                    fetchData();
                  }, [id]);

                  return (
                    <Form id="contact-form">
                      <div className="messages">
  {message && <p>{message}</p>}
</div>
                      <div className="controls">
                        <div className="form-group">
                          <Field
                            id="linkinsta"
                            type="url"
                            name="linkinsta"
                            placeholder="Url de instagram"
                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="linkfcb"
                            type="url"
                            name="linkfcb"
                            placeholder="Url de facebook"
                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="linktik"
                            type="url"
                            name="linktik "
                            placeholder="Url de tiktok"

                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="linkyou"
                            type="url"
                            name="linkyou"
                            placeholder="Url de youtube"
                          />
                        </div>
                      </div>
                    
                      <button type="submit"
                        disabled={isSubmitting}
                        className="butn bord curve mt-30" style={{ marginRight: '10px' }} onClick={handleSubmit}>
                        <span>Enregistrer</span>
                      </button>

                    </Form>
                  )
                }}
              </Formik>
            </div>
          </div>
        </div>
      </div>


    </section>
  );
};

export default Editsocmed;
