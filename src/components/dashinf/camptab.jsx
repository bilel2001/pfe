import React from 'react'
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useState } from 'react';

import { StyledBadge } from "../../../public/js/StyledBadge";
import api from '../../utils/axiosInstance';
import { pef } from 'react';
import { useRef } from 'react';




const Camptab = () => {

  const [currentPage, setCurrentPage] = useState(0);
  const perPage = 5;
  const [campagnes, setCampagnes] = useState([]);
  const localStorageRef = useRef()
  const [newCampaign, setNewCampaign] = useState("");

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`campagnes/status/users/accepter`);
        const data = response.data;
        setCampagnes(data);
        console.log(data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData()
  }, []);

  React.useEffect(() => {
    localStorageRef.current = JSON.parse(localStorage.getItem("userData"))
  }, []);

  const handleStatusChange = (e, index) => {
    const { value } = e.target;
    const newcampagnes = [...campagnes];
    newcampagnes[index].status = value;
    setcampagnes(newcampagnes);
  };


  const handleAddCampaign = () => {
    const newId = campagnes.length + 1;
    const newCampaignObj = { id: newId, name: newCampaign, status: "activé" };
    setcampagnes([...campagnes, newCampaignObj]);
    setNewCampaign("");
  };


  const handleDelete = (index) => {
    const newcampagnes = [...campagnes];
    newcampagnes.splice(index, 1);
    setcampagnes(newcampagnes);
  };
  const pages = campagnes.slice(currentPage * perPage, (currentPage + 1) * perPage);

  const handlePageClick = (data) => {
    setCurrentPage(data.selected);
  };
 
  return (

    <div>

      <div style={{
        width: '1300px',
        paddingRight: '100px',
        paddingLeft: '10px',
        marginTop: '100px',
        marginRight: '100px',
        marginLeft: '300px',
      }}>

        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '40px',
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-310px',
          }}>Table des campagnes</h3>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="color-font text-center">

                  <th scope="col">Campagnes</th>
                  <th scope="col">Action</th>
                  

                </tr>
              </thead>
              <tbody>
                {campagnes
                  .filter((campagne) =>
                    campagne.partcants.find(p => p?.demendeur?._id === localStorageRef.current?._id)
                  )
                  .map(camp => (
                    <tr key={camp._id} className="text-white text-center">

                      <td>{camp.namecamp} </td>
                      {/* <td>
                        <select
                          value={p.status}
                          onChange={(e) => handleStatusChange(e, index)}
                          style={{
                            backgroundColor: p.status === "activé" ? "#dafbe8" :
                              p.status === "en cours" ? "#fdefd8" :
                                p.status === "terminé" ? "#A3DDFF" :
                                  p.status === "annulé" ? "#fdd8e5" : "",


                            color: p.status === "activé" ? "#19a452" :
                              p.status === "en cours" ? "#ba7509" :
                                p.status === "terminé" ? "#2536ff" :
                                  p.status === "annulé" ? "#f31260" : "",

                            fontSize: "12px",
                            fontWeight: "bold",
                            border: "none",
                            padding: "10px",
                            cursor: "pointer",
                            borderRadius: "20px"
                          }}
                        >

                          <option value="activé" style={{ backgroundColor: "#dafbe8", color: "#19a452" }} >Activé</option>
                          <option value="en cours" style={{ backgroundColor: "#fdefd8", color: "#ba7509" }}>En cours</option>
                          <option value="terminé" style={{ backgroundColor: "#A3DDFF", color: "#2536ff" }}>Terminé</option>
                          <option value="annulé" style={{ backgroundColor: "#fdd8e5", color: "#f31260" }}>Annulé</option>
                        </select>
                      </td> */}
                      <td className=" d-flex justify-content-center align-items-center">
                        <button onClick={() => handleDelete(index)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                      </td>

                    </tr>))
                }
              </tbody>
            </table>





            <div className='pagination'>
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                pageCount={Math.ceil(campagnes.length / perPage)}
                onPageChange={handlePageClick}
                containerClassName={'pagination justify-content-center'}
                activeClassName={'active'}
              /></div>

          </div>

        </div>

        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginBottom: '40px',
          marginRight: '100px',
          marginLeft: '40px',
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-310px',
          }}>Ajouter campagne</h3>
          <div className="container-fluid pt-4 px-4" >

            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Campagne"
                value={newCampaign}
                onChange={(e) => setNewCampaign(e.target.value)}
              />
              <button
                className="btn btn-primary"
                type="button"
                onClick={handleAddCampaign}
              >
                Ajouter campagne
              </button>
            </div>
          </div></div>







      </div>



    </div>

  )
};

export default Camptab;
