import React from 'react'
import ReactPaginate from 'react-paginate';
import { useState } from 'react'; 

import api from '../../utils/axiosInstance';

import { useRef } from 'react';
  
const Eventtab = () => {

  const [campagnes, setCampagnes] = useState([]);
  const localStorageRef = useRef()
  const [newCampaign, setNewCampaign] = useState("");

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`events/status/users/accepter`);
        const data = response.data;
        setCampagnes(data);
        console.log(data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData()
  }, []);

  React.useEffect(() => {
    localStorageRef.current = JSON.parse(localStorage.getItem("userData"))
  }, []);



  const invitationsData = [

    {id: '1' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital' },
    {id: '2' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital'},
    {id: '3' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital'},
    {id: '4' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital'},
    {id: '5' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital'},
    {id: '6' ,titre: 'InfluenX', organisateur: 'emna', date:'10/10/2020',lieu:'sfax',domaine:'digital'}
  ];
  const [currentPage, setCurrentPage] = useState(0);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };
  
  
  



  const [invitations, setInvitations] = useState(invitationsData);
  const [tableAccepted, setTableAccepted] = useState([]);

  const invitationsPerPage = 2;
    const startIndex = currentPage * invitationsPerPage;
    const endIndex = startIndex + invitationsPerPage;
    const invitationsToDisplay = tableAccepted.slice(startIndex, endIndex);

  function acceptInvitation(invitation) {
    setInvitations((prevInvitations) => prevInvitations.filter((prevInvitation) => prevInvitation !== invitation));
    const newTableAccepted = [...tableAccepted, invitation];
    setTableAccepted(newTableAccepted);
  }

  function rejectInvitation(invitation) {
    setInvitations((prevInvitations) => prevInvitations.filter((prevInvitation) => prevInvitation !== invitation));
  }
  
    
  return (
<div>
    <div className="container-fluid pt-4 px-4"
    style={{
      width: '1100px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '50px',
      marginRight: '100px',
      marginLeft: '350px',
    }}
  >

{/*tableau*/}
<div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <h3 className="color-font" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
         marginTop: '-10px',
         marginBottom: '10px',
          marginRight: '100px',
          marginLeft: '-340px',
        }}>Évènements visités</h3>
          <div className="table-responsive">
          <table className="table text-start align-middle table-bordered table-hover mb-0">
          <thead>
                <tr className="color-font text-center">

                  <th scope="col">Titre</th>
                  <th scope="col">Organisateur</th>
                  <th scope="col">Date</th>
                  <th scope="col">
                    Lieu
                  </th>
                  <th scope="col">Domaine</th>
                  

                </tr>
                </thead>
                <tbody>
                {campagnes
                  .filter((campagne) =>
                    campagne.partcants.find(p => p?.demendeur?._id === localStorageRef.current?._id)
                  )
                  .map(camp => (
            
              <tr key={camp._id} className='text-white'>
                
                 <td>{camp.title}          </td>
                 <td>{camp.organiseurevent}   </td>
                 <td>{camp.dated}           </td>
                 <td>{camp.lieu}           </td>
                 <td>{camp.domainevent}        </td>
                 </tr>
                
          ))} 
          </tbody>
          </table>     
          <div className='pagination'>
            <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        pageCount={Math.ceil(tableAccepted.length / invitationsPerPage)}
        
        onPageChange={handlePageChange}
        containerClassName={'pagination justify-content-center'}
        activeClassName={'active'}
      /></div>
             
          </div>

        </div>




          {/*invitations*/}

       

    </div> 
    </div>
  )
};

export default Eventtab;
