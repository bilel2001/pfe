
import React, { useState } from 'react';

import ReactPaginate from 'react-paginate';



const Rechercheheader = () => {
  
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10;


  // Initialize the data 



  const data = [
    { username: 'bile elloumi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'emna chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '1000k-5000K' },
    { username: 'abir chabchoub', genre: 'homme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'ahmed louati', genre: 'femme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },
    { username: 'bile elloumi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'emna chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '1000k-5000K' },
    { username: 'abir chabchoub', genre: 'homme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'ahmed louati', genre: 'femme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },{ username: 'bile elloumi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'emna chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '1000k-5000K' },
    { username: 'abir chabchoub', genre: 'homme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'ahmed louati', genre: 'femme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },{ username: 'bile elloumi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'emna chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '1000k-5000K' },
    { username: 'abir chabchoub', genre: 'homme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'ahmed louati', genre: 'femme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },
    { username: 'imen louati', genre: 'homme', pays: 'Tunisie', réseau: 'facebook', domaine: 'sport', followers: '+10000K' }
  ];
  const [filter1, setFilter1] = useState('all');
  const [filter, setFilter] = useState('all');
  const [searchTerm, setSearchTerm] = useState('');
  const [filter2, setFilter2] = useState('all');
  const [filter3, setFilter3] = useState('all');
  const [filter4, setFilter4] = useState('all');


  //handelfilters
  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };
  const handleFilterChange1 = (event) => {
    setFilter1(event.target.value);

  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value.toLowerCase());
  };
  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  // Filter data based on current filter and search term
  const filteredData1 = data.filter((item) => {
    if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return item.username.toLowerCase().includes(searchTerm.toLowerCase());


      //1-4
    } else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    } else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.domaine === filter1 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.réseau === filter2 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    //2-3

    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.réseau === filter2 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.genre === filter &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.domaine === filter1 &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.domaine === filter1 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (

        item.réseau === filter2 &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (

        item.réseau === filter2 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (

        item.followers === filter3 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }


    //3-2

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.réseau === filter2 &&
        item.domaine === filter1 &&
        item.followers === filter3 &&

        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.domaine === filter2 &&
        item.réseau === filter3 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.domaine === filter2 &&
        item.réseau === filter3 &&
        item.genre === filter &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.pays === filter4 &&
        item.réseau === filter3 &&
        item.genre === filter &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.pays === filter4 &&
        item.followers === filter3 &&
        item.domaine === filter1 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }








    //4-1

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.followers === filter3 &&
        item.réseau === filter2 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.pays === filter4 &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.genre === filter &&
        item.réseau === filter2 &&
        item.pays === filter4 &&
        item.followers === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (

        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.followers === filter3 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else {
      return (
        item.genre === filter &&
        item.domaine === filter1 &&
        item.réseau === filter2 &&
        item.followers === filter3 &&
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
  });

  return (
    <div>

      {/* Display the filtered table data */}
      <div className="container-fluid pt-4 px-4"
        style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '0px',
          marginRight: '100px',
          marginLeft: '350px',
        }}
      >
        {/* Add the input and dropdown for filtering */}
        <div className="d-flex justify-content-between mb-4">
          <div className="d-flex align-items-center">


            {/* recherche */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '283px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '70px',
                marginRight: '0px',
                marginLeft: '-20px',
              }}
            >

              <input
                type="text"
                placeholder="Entrer le nom d'utilisateur"
                className="form-control me-2"
                onChange={handleSearch}
              />
            </div>

            {/* domaine */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '260px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '70px',
                marginRight: '0px',
                marginLeft: '-40px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter1}
                onChange={handleFilterChange1}
              >
                <option value="all">Tous les Domaines</option>
                <option value="lifestyle">Lifestyle</option>
                <option value="sport">Sport</option>
              </select>
            </div>



           
            {/* domaines */}
           

            

            
              <div className="line right " style={{
                width: '700px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '190px',
                marginRight: '0px',
                marginLeft: '-1125px',
              }}></div>
            

          </div>

        </div>


        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '100px',

          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="color-font text-center">

                  <th scope="col">Nom d'utilisateur</th>
                  
                  <th scope="col">Domaine</th>
                  <th scope="col">Action</th>
                  

                </tr>
              </thead>
              <tbody>
                {/* Map over the filtered data instead of the original data */}
                {filteredData1.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage).map((item) => (
                  <tr key={item.id} className='text-white'>

                    <td>{item.username}</td>
                   
                    <td>{item.domaine}</td>
                   
                    <td><a className="btn btn-sm btn-primary" href="#">Voir profil</a></td>
                  </tr>
                ))}
                
              </tbody>
              <div className="d-flex justify-content-end">
    
</div>
            </table>
            <div className='pagination'>
            <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        pageCount={Math.ceil(filteredData1.length / itemsPerPage)}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageChange}
        containerClassName={'pagination justify-content-center'}
        activeClassName={'active'}
      /></div>
          </div>

        </div>
      </div>
    </div>
  );
};

export default Rechercheheader;