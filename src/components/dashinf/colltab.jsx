import React from 'react'
import ReactPaginate from 'react-paginate';
import { useState } from 'react';
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router';
import { useRef } from 'react';



const Colltab = ({ profilId }) => {

  //const [collaborations, setCollaborations] = useState(users);
  const [invitations, setInvitations] = useState([]);
  const [tableAccepted, setTableAccepted] = useState([]);
  const [acceptations, setAcceptations] = useState([]);
  const [success, setSuccess] = useState(false);
  const localStorageRef = useRef()
  const [currentPage, setCurrentPage] = useState(0);
  const perPage = 5;
  const [users, setUsers] = useState([
    { id: 1, name: "influenceur 1", campaign: "bla bla", status: "activé" },
    { id: 2, name: "influenceur 2", campaign: "bla bla", status: "en cours" },
    { id: 3, name: "influenceur 3", campaign: "bla bla", status: "terminé" },
    { id: 4, name: "influenceur 4", campaign: "bla bla", status: "annulé" },
  ]);

  // const [newCampaign, setNewCampaign] = useState("");

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`users/status/users`);
        const data = response.data;
        console.log(data);
        setInvitations(data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData().then(() => setSuccess(false));
  }, [success]);

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`users/status/users/accepter`);
        const data = response.data;
        setAcceptations(data);
        console.log(data)

        console.log(data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData().then(() => setSuccess(false))
  }, [success]);


  React.useEffect(() => {
    localStorageRef.current = JSON.parse(localStorage.getItem("userData"))
  }, []);

  const handleStatusChange = (e, index) => {
    const { value } = e.target;
    const newUsers = [...users];
    newUsers[index].status = value;
    setUsers(newUsers);
  };


  // const handleAddCampaign = () => {
  //   const newId = users.length + 1;
  //   const newCampaignObj = { id: newId, name: newCampaign, status: "activé" };
  //   setUsers([...users, newCampaignObj]);
  //   setNewCampaign("");
  // };


  const handleDelete = (index) => {
    const newUsers = [...users];
    newUsers.splice(index, 1);
    setUsers(newUsers);
  };
  const pages = users.slice(currentPage * perPage, (currentPage + 1) * perPage);

  const handlePageClick = (data) => {
    setCurrentPage(data.selected);
  };
  //demande de collaboration



  const invitationsPerPage = 2;
  const startIndex = currentPage * invitationsPerPage;
  const endIndex = startIndex + invitationsPerPage;
  const invitationsToDisplay = tableAccepted.slice(startIndex, endIndex);

  const acceptInvitation = async (e, collaborateurId) => {
    console.log(typeof localStorageRef.current)
    e.preventDefault()
    await api.put(`users/status/${collaborateurId}/${localStorageRef.current?._id}`).then(() => setSuccess(true))
  }

  function rejectInvitation(invitation) {
    setInvitations((prevInvitations) => prevInvitations.filter((prevInvitation) => prevInvitation !== invitation));
    setSuccess(true)

  }



  return (

    <div>

      <div style={{
        width: '1300px',
        paddingRight: '100px',
        paddingLeft: '10px',
        marginTop: '100px',
        marginRight: '100px',
        marginLeft: '300px',
      }}>

        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '40px',
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-290px',
          }}>Table des collaborations</h3>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="color-font text-center">

                  <th scope="col">Annonceurs</th>
                  <th scope="col">Prénom</th>
                  <th scope="col">Statut</th>
                  <th scope="col">Action</th>

                </tr>
              </thead>
              <tbody>
                {acceptations.map((acceptations, index) => (
                  acceptations.collaborateurs.map(part => (

                    <tr key={part._id} className="text-white text-center">

                      <td>{part.demendeur?.name} </td>
                      <td>{part.demendeur?.lastname} </td>
                      <td>
                        <select
                          value={users[index].status}
                          onChange={(e) => handleStatusChange(e, index)}
                          style={{
                            backgroundColor: users[index].status === "activé" ? "#dafbe8" :
                              users[index].status === "en cours" ? "#fdefd8" :
                                users[index].status === "terminé" ? "#A3DDFF" :
                                  users[index].status === "annulé" ? "#fdd8e5" : "",


                            color: users[index].status === "activé" ? "#19a452" :
                              users[index].status === "en cours" ? "#ba7509" :
                                users[index].status === "terminé" ? "#2536ff" :
                                  users[index].status === "annulé" ? "#f31260" : "",

                            fontSize: "12px",
                            fontWeight: "bold",
                            border: "none",
                            padding: "10px",
                            cursor: "pointer",
                            borderRadius: "20px"
                          }}
                        >

                          <option value="activé" style={{ backgroundColor: "#dafbe8", color: "#19a452" }}> Activé   </option>
                          <option value="en cours" style={{ backgroundColor: "#fdefd8", color: "#ba7509" }}> En cours </option>
                          <option value="terminé" style={{ backgroundColor: "#A3DDFF", color: "#2536ff" }}> Terminé  </option>
                          <option value="annulé" style={{ backgroundColor: "#fdd8e5", color: "#f31260" }}> Annulé   </option>
                        </select>
                      </td>
                      <td >
                        <button onClick={() => handleDelete(index)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                        <button onClick={() => handleDelete(index)} className="btn btn-info" style={{
                          marginLeft: '10px',
                          width: '128px'
                        }}> <i className="fas fa-edit"></i> Modifier</button>
                      </td>

                    </tr>

                  ))

                ))}
              </tbody>
            </table>


            {/* pagination */}
            <div className='pagination'>
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                pageCount={Math.ceil(users.length / perPage)}
                onPageChange={handlePageClick}
                containerClassName={'pagination justify-content-center'}
                activeClassName={'active'}
              />
            </div>
          </div>

        </div>
        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '40px',
          marginBottom: '100px'
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-250px',
          }}>
            Demandes de collaborations
          </h3>


          <div className="row g-4">
            <div className="col-sm-12">
              <div className="h-100 bg1 rounded p-4">
                {invitations.map((invitation, index) => (

                  invitation.collaborateurs
                    .filter(part => part.status !== 'accepter')
                    .map(part => (
                      <div key={index} className="d-flex align-items-center border-bottom py-3">
                        <div className="w-100 ms-3">
                          <div className="d-flex w-100 justify-content-between">
                            <h6 className="mb-0">{part?.demendeur?.name}</h6>
                            <h6 className="mb-0">{part.demendeur?.domain}</h6>

                          </div>

                        </div>
                        <div className="d-flex align-items-center justify-content-center ms-auto ml-5">
                          <button className='btn'>
                            consulter</button>
                          <button
                            onClick={(e) => acceptInvitation(e, part?.demendeur?._id)}
                          >
                            <i className="bi bi-check-circle-fill text-success me-2" style={{ fontSize: 24 }} ></i>

                          </button>
                          <i className="bi bi-x-circle-fill text-danger" style={{ fontSize: 24 }} onClick={() => rejectInvitation(invitation)}></i>

                        </div>
                      </div>
                    ))

                ))}


              </div>
            </div>

          </div>

        </div>

      </div>
    </div>
  )
};

export default Colltab;








