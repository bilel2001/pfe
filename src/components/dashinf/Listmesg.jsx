import React from 'react';
import Link from "next/link";

const Listmesg = () => {
    return (
        <div>

            <div className="container-fluid pt-4 px-4" style={{
                width: '1050px',
                paddingRight: '100px',
                paddingLeft: '10px',
                marginTop: '25px',
                marginBottom: '25px',
                marginRight: '100px',
                marginLeft: '400px',
            }}
            >
                <div className="row g-4">
                    <div className="col-sm-12">
                        <div className="h-100 bg1 rounded p-4">
                            <div className="d-flex align-items-center justify-content-between mb-2">
                                <h6 className="mb-0">Messages</h6>
                                <Link href="/Influenceur/boîte_de_réception">
                                    <a href>Show All</a>
                                </Link>
                            </div>

                            <div className="d-flex align-items-center border-bottom py-3">
                                <img className="rounded-circle flex-shrink-0" src="img/user.jpg" alt style={{ width: 40, height: 40 }} />
                                <div className="w-100 ms-3">
                                    <div className="d-flex w-100 justify-content-between">
                                        <h6 className="mb-0">Jhon Doe</h6>
                                        <small>15 minutes ago</small>
                                    </div>
                                    <span>Short message goes here...</span>
                                </div>
                            </div>
                            <div className="d-flex align-items-center border-bottom py-3">
                                <img className="rounded-circle flex-shrink-0" src="img/user.jpg" alt style={{ width: 40, height: 40 }} />
                                <div className="w-100 ms-3">
                                    <div className="d-flex w-100 justify-content-between">
                                        <h6 className="mb-0">Jhon Doe</h6>
                                        <small>15 minutes ago</small>
                                    </div>
                                    <span>Short message goes here...</span>
                                </div>
                            </div>
                            <div className="d-flex align-items-center border-bottom py-3">
                                <img className="rounded-circle flex-shrink-0" src="img/user.jpg" alt style={{ width: 40, height: 40 }} />
                                <div className="w-100 ms-3">
                                    <div className="d-flex w-100 justify-content-between">
                                        <h6 className="mb-0">Jhon Doe</h6>
                                        <small>15 minutes ago</small>
                                    </div>
                                    <span>Short message goes here...</span>
                                </div>
                            </div>
                            <div className="d-flex align-items-center pt-3">
                                <img className="rounded-circle flex-shrink-0" src="img/user.jpg" alt style={{ width: 40, height: 40 }} />
                                <div className="w-100 ms-3">
                                    <div className="d-flex w-100 justify-content-between">
                                        <h6 className="mb-0">Jhon Doe</h6>
                                        <small>15 minutes ago</small>
                                    </div>
                                    <span>Short message goes here...</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
    )
}

export default Listmesg
