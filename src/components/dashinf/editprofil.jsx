import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, useFormikContext } from "formik";
import axios from 'axios';

const Editprofil = () => {
  const [file, setFile] = useState()
  const [message, setMessage] = useState("");

  let id = '';


  if (typeof window !== 'undefined') {
    const userData = localStorage.getItem('userData');
    const parsedUserData = JSON.parse(userData);
    id = parsedUserData._id;

  }

  const handleSubmit = async (values) => {
   // console.log(JSON.stringify(values))

    try {
      const instance = axios.create({ baseURL: 'http://localhost:3001' });
      delete instance.defaults.headers.common['Authorization'];
      const { name, lastname, domain, email, gender, country, dateOfBitrh, photo, phone, description } = values

      await instance.put("api/users", { id, name, lastname, domain, email, gender, country, dateOfBitrh, photo, phone, description });
      setMessage("ajoute les element avec succès");

    } catch (error) {
      console.log(error);
    }
  };




  return (
    <section className="contact section-padding" style={{
      width: '950px',
      paddingRight: '100px',
      paddingLeft: '10px',

      marginRight: '100px',
      marginLeft: '335px',
    }} >
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Modifier le profil.</h4>
              <Formik
                initialValues={{
                  name: '',
                  lastname: "",
                  email: "",
                  domain: "",
                  gender: null,
                  country: "",
                  dateOfBitrh: "",
                  photo: "",
                  phone: "",
                  description: ""
                }}
                onSubmit={handleSubmit}
              >
                {({ errors, touched, isSubmitting, setFieldValue }) => {
                  const [user, setUser] = useState({});
                  const [showPassword, setShowPassword] = useState(false);

                  useEffect(() => {
                    const fetchData = async () => {
                      try {
                        const instance = axios.create({ baseURL: 'http://localhost:3001' });
                        delete instance.defaults.headers.common['Authorization'];
                        const { data } = await instance.get('api/users', { params: { id } });
                        const fields = ['name', 'lastname', 'email', 'role','domain','gender','phone'];
                        fields.forEach(field => setFieldValue(field, data[field], false));
                      } catch (error) {
                        console.log(error);
                      }
                    };
                    fetchData();
                  }, [id]);

                  return (
                    <Form id="contact-form">
                                            <div className="messages">
  {message && <p>{message}</p>}
</div>
                      <div className="controls">
                        <div className="form-group">
                          <Field
                            id="name"
                            type="text"
                            name="name"
                            placeholder="Nom"
                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="lastname"
                            type="text"
                            name="lastname"
                            placeholder="Prénom"
                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="domain"
                            type="text"
                            name="domain "
                            placeholder="Domaine"
                          />
                        </div>
                        <div className="form-group">
                          <Field
                            id="email"
                            type="email"
                            name="email"
                            placeholder="Email"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <Field
                          id="country"
                          type="text"
                          name="country"
                          placeholder="Pays"


                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="file"
                          id="photo"
                          name='photo'
                          onChange={(event) => {


                          }}
                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="phone"
                          type="text"
                          name="phone"
                          placeholder="Numéro de téléphone"

                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="dateOfBitrh"
                          type="date"
                          name="dateOfBitrh"
                          placeholder="Date de naissance"


                        />
                      </div>
                      <div className="form-group">
                        <Field
                          id="description"
                          type="text"
                          name="description"
                          placeholder="Description*"


                        />
                      </div>
                      <div className="form-group">
                        <label>Genre:</label>
                        <div style={{ display: "flex", flexDirection: "row-reverse" }}>
                          <div style={{ marginLeft: "10px" }}>
                            <label>
                              <Field type="radio" name="gender" value="femme" />
                              Femme
                            </label>
                          </div>
                          <div>
                            <label>
                              <Field type="radio" name="gender" value="homme" />
                              Homme
                            </label>
                          </div>
                        </div></div>

                      <button type="submit"
                        disabled={isSubmitting}
                        className="butn bord curve mt-30" style={{ marginRight: '10px' }} onClick={handleSubmit}>
                        <span>Enregistrer</span>
                      </button>

                    </Form>
                  )
                }}
              </Formik>
            </div>
          </div>
        </div>
      </div>


    </section>
  );
};

export default Editprofil;

