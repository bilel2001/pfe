import React, { useEffect, useState } from 'react';

import { Formik, Form, Field } from "formik";

import axios from 'axios';
const Editmdp = () => {

  const [password, setPassword] = useState('');

  let id = '';


  if (typeof window !== 'undefined') {
    const userData = localStorage.getItem('userData');
    const parsedUserData = JSON.parse(userData);
    id = parsedUserData._id;
    console.log(id)
  }



  const handleSubmit = async (values) => {
    console.log(id)
    console.log(JSON.stringify(values));

    try {
      const instance = axios.create({ baseURL: 'http://localhost:3001' });
      delete instance.defaults.headers.common['Authorization'];
      const { password, password1, password2 } = values
      console.log(id)
      await instance.put("api/users/update-password", { id, password, password1, password2 });

      // Reset the form values
      values.password = '';
      values.password1 = '';
      values.password2 = '';



    } catch (error) {
      console.log(error);
    }
  };


  return (
    <section className="contact section-padding" style={{
      width: '950px',
      paddingRight: '100px',
      paddingLeft: '10px',

      marginRight: '100px',
      marginLeft: '335px',
    }} >
      <div className="container">
        <div className="row">
          <div className="col-lg-6">

          </div>

          <div className="col-lg-6" style={{

            marginTop: ' 50px',

          }}>
            <div className="form md-mb50">
              <h4 className="fw-700 color-font mb-50">Mot de passe</h4>
              <Formik
                initialValues={{
                  password: "",
                  password1: "",
                  password2: "",

                }}
                onSubmit={handleSubmit}
              >
                <Form id="contact-form">
                  <div className="messages" ></div>
                  <div className="controls">
                    <div className="form-group">
                      <Field
                        id="password"
                        type="password"
                        name="password"
                        placeholder="Ancien Mot de passe"
                      />
                    </div>
                    <div className="form-group">
                      <Field
                        id="password1"
                        type="password"
                        name="password1"

                        placeholder="nouveau Mot de passe"
                      />
                    </div>
                    <div className="form-group">
                      <Field
                        id="password2"
                        type="password"
                        name="password2"
                        placeholder="Répéter Mot de passe"
                      />
                    </div>

                  </div>


                  <button type="submit" className="butn bord curve mt-30" style={{ marginRight: '10px' }}>
                    <span>Enregistrer</span>
                  </button>

                </Form>
              </Formik>
            </div>
          </div>
          <div className="col-lg-6" style={{
            width: '950px',
            paddingRight: '0px',
            paddingLeft: '10px',
            marginTop: ' -390px',
            marginRight: '0px',
            marginLeft: '500px',




          }}>

          </div>
        </div>
      </div>


    </section>
  );
};

export default Editmdp;
