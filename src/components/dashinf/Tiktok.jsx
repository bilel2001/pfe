import React, { useEffect } from 'react';
import Chart from 'chart.js/auto';




const Tiktok = () => {
    useEffect(() => {
        // Chart Global Color
        Chart.defaults.color = "#6C7293";
        Chart.defaults.borderColor = "#000000";

        var ctx8 = document.getElementById("donut-chart").getContext("2d");
var myChart8 = new Chart(ctx8, {
    type: "doughnut",
    data: {
        labels: ["Hommes", "Femmes"],
        datasets: [{
            backgroundColor: [
                "rgba(235, 22, 22, .7)",
                "rgba(52, 152, 219, .7)",
                
            ],
            data: [40, 55, 5]
        }]
    },
    options: {
        responsive: true,
        legend: {
            position: "bottom"
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                        return previousValue + currentValue;
                    });
                    var currentValue = dataset.data[tooltipItem.index];
                    var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                    return dataset.labels[tooltipItem.index] + ': ' + percentage + '%';
                }
            }
        }
    }
});

        var ctx3 = document.getElementById("bar-chart").getContext("2d");
var myChart3 = new Chart(ctx3, {
    type: "bar",
    data: {
        labels: ["France", "Spain", "Italy", "Germany"],
        datasets: [{
            label: "Homme",
            backgroundColor: "rgba(235, 22, 22, .7)",
            data: [10, 15, 20, 8]
        },
        {
            label: "Femme",
            backgroundColor: "rgba(52, 152, 219, .7)",
            data: [8, 12, 18, 7]
        }]
    },
    options: {
        responsive: true
    }
});

var ctx4 = document.getElementById("bar1-chart").getContext("2d");
var myChart4 = new Chart(ctx4, {
    type: "bar",
    data: {
        labels: ["18-24", "25-34", "35-44", "45-54", "55+"],
        datasets: [{
            label: "Homme",
            backgroundColor: "rgba(235, 22, 22, .7)",
            data: [10, 15, 20, 8, 5]
        },
        {
            label: "Femme",
            backgroundColor: "rgba(52, 152, 219, .7)",
            data: [8, 12, 18, 7, 3]
        }]
    },
    options: {
        responsive: true
    }
});







var ctx5 = document.getElementById("line-chart").getContext("2d");
var myChart5 = new Chart(ctx5, {
    type: "line",
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
            label: "Number of Subscribers",
            borderColor: "rgba(75, 192, 192, 1)",
            data: [500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600],
            fill: false
        }]
    },
    options: {
        responsive: true
    }
});

    }, []);

    return (
        <div>
            <div className="container-fluid pt-4 px-4" style={{
                width: '1050px',
                paddingRight: '100px',
                paddingLeft: '10px',
                marginTop: '70px',
                marginRight: '280px',
                marginLeft: '300px',
            }}>
                <h4 className="wow fadeIn color-font" data-wow-delay=".5s">
Tiktok          </h4>
                <div className="row g-4">
                <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h6 className="mb-4">Répartition par genre </h6>
                            <canvas id="donut-chart" />
                        </div>
                    </div>
                    <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h6 className="mb-4">Nombre d'abonnées par pays</h6>
                            <canvas id="bar-chart" />
                        </div>
                    </div>
                   
                </div>
                <div className="row g-4"style={{
                
                marginTop: '20px',
                
            }}>
                <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h6 className="mb-4">Nombre d'abonnées par mois </h6>
                            <canvas id="line-chart" />
                        </div>
                    </div>
                    <div className="col-sm-12 col-xl-6">
                        <div className="bg1 rounded h-100 p-4">
                            <h6 className="mb-4">Nombre d'abonnées par age</h6>
                            <canvas id="bar1-chart" />
                        </div>
                    </div>
                   
                </div>
                
                
            </div>
        </div>
    );
};

export default Tiktok;
