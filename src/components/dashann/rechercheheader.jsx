
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import ReactPaginate from 'react-paginate';
import axios from 'axios';


const Rechercheheader = () => {
  
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10;

  const [filteredUsers, setFilteredUsers] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const instance = axios.create({ baseURL: 'http://localhost:3001' });
        delete instance.defaults.headers.common['Authorization'];
        const role = 'influenceur';
        const { data } = await instance.get('/api/users/role', { params: { role } });
        console.log(data);
        setFilteredUsers(data);
        
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);
  // Initialize the data 



  const data = [
    { username: 'Bilel elloumi', genre: 'homme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'Emna chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'fashion', followers: '1000k-5000K' },
    { username: 'Abir chabchoub', genre: 'femme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'Ahmed louati', genre: 'homme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },
    { username: 'Imen chouikhi', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'Bilel chabchoub', genre: 'homme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
    { username: 'Emna louati', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '1000k-5000K' },
    { username: 'Abir trigui', genre: 'femme', pays: 'Allemagne', réseau: 'instagram', domaine: 'lifestyle', followers: '5000K-10000K' },
    { username: 'Ahmed elloumi', genre: 'homme', pays: 'France', réseau: 'tiktok', domaine: 'sport', followers: '100k' },
    { username: 'Imen Louati', genre: 'femme', pays: 'Tunisie', réseau: 'instagram', domaine: 'lifestyle', followers: '100k-1000K' },
  ];
  const [filter1, setFilter1] = useState('all');
  const [filter, setFilter] = useState('all');
  const [searchTerm, setSearchTerm] = useState('');
  const [filter2, setFilter2] = useState('all');
  const [filter3, setFilter3] = useState('all');
  const [filter4, setFilter4] = useState('all');


  //handelfilters
  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };
  const handleFilterChange1 = (event) => {
    setFilter1(event.target.value);

  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value.toLowerCase());
  };
  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  // Filter data based on current filter and search term
  const filteredData1 = filteredUsers.filter((item) => {
    if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return item.name.toLowerCase().includes(searchTerm.toLowerCase());


      //1-4
    } else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    } else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.domain === filter1 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.socmeda === filter2 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.nbfollower === filter3 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.pays === filter4 &&
        item.username.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    //2-3

    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.socmeda === filter2 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.gender === filter &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.domain === filter1 &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.domain === filter1 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (

        item.socmeda === filter2 &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (

        item.socmeda === filter2 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (

        item.nbfollower === filter3 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }


    //3-2

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.socmeda === filter2 &&
        item.domain === filter1 &&
        item.nbfollower === filter3 &&

        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.domain === filter2 &&
        item.socmeda === filter3 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.domain === filter2 &&
        item.socmeda === filter3 &&
        item.gender === filter &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.country === filter4 &&
        item.socmeda === filter3 &&
        item.gender === filter &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.country === filter4 &&
        item.nbfollower === filter3 &&
        item.domain === filter1 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }








    //4-1

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 === 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.nbfollower === filter3 &&
        item.socmeda === filter2 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 === 'all' && filter4 !== 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter !== 'all' && filter1 !== 'all' && filter2 === 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.country === filter4 &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }

    else if (filter !== 'all' && filter1 === 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (
        item.gender === filter &&
        item.socmeda === filter2 &&
        item.country === filter4 &&
        item.nbfollower === filter3 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else if (filter === 'all' && filter1 !== 'all' && filter2 !== 'all' && filter3 !== 'all' && filter4 !== 'all') {
      return (

        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.nbfollower === filter3 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
    else {
      return (
        item.gender === filter &&
        item.domain === filter1 &&
        item.socmeda === filter2 &&
        item.nbfollower === filter3 &&
        item.country === filter4 &&
        item.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
  });

  return (
    <div>

      {/* Display the filtered table data */}
      <div className="container-fluid pt-4 px-4"
        style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '0px',
          marginRight: '100px',
          marginLeft: '350px',
        }}
      >
        {/* Add the input and dropdown for filtering */}
        <div className="d-flex justify-content-between mb-4">
          <div className="d-flex align-items-center">


            {/* recherche */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '283px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '0px',
                marginRight: '0px',
                marginLeft: '-20px',
              }}
            >

              <input
                type="text"
                placeholder="Entrer le nom d'utilisateur"
                className="form-control me-2"
                onChange={handleSearch}
              />
            </div>

            {/* genre */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '230px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '0px',
                marginRight: '0px',
                marginLeft: '-40px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter}
                onChange={handleFilterChange}
              >

                <option value="all">Tous les Genres</option>
                <option value="femme">Femme</option>
                <option value="homme">Homme</option>
              </select>
            </div>



            {/* pays */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '210px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '0px',
                marginRight: '0px',
                marginLeft: '-40px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter4}
                onChange={(e) => setFilter4(e.target.value)}
              >
                <option value="all">Tous les Pays</option>
                <option value="Tunisie">Tunisie</option>
                <option value="France">France</option>
                <option value="Allemagne">Allemagne</option>
              </select>
            </div>



            {/* réseaux sociaux */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '310px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '0px',
                marginRight: '0px',
                marginLeft: '-40px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter2}
                onChange={(e) => setFilter2(e.target.value)}
              >
                <option value="all">Tous les Réseaux Sociaux</option>
                <option value="instagram">Instagram</option>
                <option value="facebook">Facebook</option>
                <option value="tiktok">Tiktok</option>
              </select>

            </div>


            {/* domaines */}
            <div className="container-fluid pt-4 px-4"
              style={{
                width: '250px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '120px',
                marginRight: '0px',
                marginLeft: '-915px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter1}
                onChange={handleFilterChange1}
              >
                <option value="all">Tous les Domaines</option>
                <option value="lifestyle">Lifestyle</option>
                <option value="sport">Sport</option>
              </select>
            </div>

            {/* followers */}

            <div className="container-fluid pt-4 px-4"
              style={{
                width: '270px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '120px',
                marginRight: '0px',
                marginLeft: '-40px',
              }}
            >
              <select
                className="form-select w-100"
                value={filter3}
                onChange={(e) => setFilter3(e.target.value)}
              >
                <option value="all">Nombre de Followers</option>
                <option value="100k-1000K">100k-1000K</option>
                <option value="1000k-5000K">1000k-5000K</option>
                <option value="5000K-10000K">5000K-10000K</option>
                <option value="+10000K">+10000K</option>
              </select>
              <div className="line right " style={{
                width: '700px',
                paddingRight: '0px',
                paddingLeft: '10px',
                marginTop: '50px',
                marginRight: '0px',
                marginLeft: '-1125px',
              }}></div>
            </div>

          </div>

        </div>


        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '100px',

          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="text-white">

                  <th scope="col">Nom d'utilisateur</th>
                  <th scope="col">Genre</th>
                  <th scope="col">Pays</th>
                  <th scope="col">Réseau Social</th>
                  <th scope="col">Domaine</th>
                  <th scope="col">Followers</th>
                  <th scope="col">Profil</th>

                </tr>
              </thead>
              <tbody>
                {/* Map over the filtered data instead of the original data */}
                {filteredData1.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage).map((item) => (
                  <tr key={item.id}>

                    <td>{item.name} {item.lastname}</td>
                    <td>{item.gender}</td>
                    <td>{item.country}</td>
                    <td>{item.socmeda}</td>
                    <td>{item.domain}</td>
                    <td>{item.nbfollower}</td>
                    <td><a className="btn btn-sm btn-primary" href={`/profils/${item._id}`} >Voir profil</a></td>
                  </tr>
                ))}
                
              </tbody>
              <div className="d-flex justify-content-end">
    
</div>
            </table>
            <div className='pagination'>
            <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        pageCount={Math.ceil(filteredData1.length / itemsPerPage)}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageChange}
        containerClassName={'pagination justify-content-center'}
        activeClassName={'active'}
      /></div>
          </div>

        </div>
      </div>
    </div>
  );
};

export default Rechercheheader;