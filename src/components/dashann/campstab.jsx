import React from 'react'
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useState } from 'react'; 
import api from "../../utils/axiosInstance";



  
const Campstab = () => {
  const [invitations, setInvitations] = useState([]);
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`campagnes`);
        const data = response.data;
        console.log(data);
        setInvitations(data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData(); 

  }, []);


  const handleDelete = async (e, campagneId) => {
    e.preventDefault()
    await api.delete(`campagnes/${campagneId}/delete`).then(() => setSuccess(true))
  }

  const events  = [

    {id: '1' ,title: 'Cosmo techno', description:'Sachez toujours que votre enfant est en sécurité',  domains:'Digital', socmedia:'Instagram' , nbfollowers:'20K' , pays:'Sousse'   },
    {id: '2' ,title: 'Glocalme', description:'Toujours connecté : libérez votre productivité avec les données mobiles GlocalMe',  domains:'Digital', socmedia:'Instagram' , nbfollowers:'21K' , pays:'Hammamet'     },
    {id: '3' ,title: 'Glocalme', description:'Restez toujours connecté',  domains:'Digital', socmedia:'Instagram' , nbfollowers:'22K' , pays:'Tunis'    },
  ];
  const [currentPage, setCurrentPage] = useState(0);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };
  const itemsPerPage = 3;
  
  const eventsToDisplay = events.slice(
    currentPage * itemsPerPage,
    (currentPage + 1) * itemsPerPage
  );

  

 

  
  
    
  return (
<div>
    <div className="container-fluid pt-4 px-4"
    style={{
      width: '1100px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '50px',
      marginRight: '100px',
      marginLeft: '327px',
      marginBottom: '-100px',
    }}
  >

{/*tableau*/}
<div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '-100px',
          marginBottom: '0px',
          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <h3 className="color-font" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
         marginTop: '-10px',
         marginBottom: '10px',
          marginRight: '100px',
          marginLeft: '-315px',
        }}>Mes campagnes</h3>
          <div className="table-responsive">
          <table className="table text-start align-middle table-bordered table-hover mb-0">
          <thead>
                <tr className="text-center color-font ">

                  <th scope="col">Titre</th>
                  
                  <th scope="col">Description</th>
                  
                  <th scope="col">Domaines</th>
                  <th scope="col">Réseaux sociaux</th>
                  <th scope="col">Nombre d'abonnés</th>
                  <th scope="col">Pays</th>
                  <th scope="col">Action</th>


                  

                </tr>
                </thead>
                <tbody>
           {invitations.map((event, index) => (
            
              <tr key={event.id} className="text-white">
                
                 <td>{event.namecamp}          </td>
                 <td>{event.descriptioncamp}        </td>
                
                 <td>{event.domaincamp}        </td>
                 <td>{event. reseausoc}        </td>
                 <td>{event.nbfollower}        </td>
                 <td>{event.countrycamp}        </td>

                 <td className=" grid">
                      <button onClick={(e) => handleDelete(e, event?._id)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                      <button onClick={() => handleDelete(index)} className="btn btn-info" style={{marginTop:'10px',
                    width:'128px'}}> <i className="fas fa-edit"></i> Modifier</button>
                    </td>
                 </tr>
                
          ))} 
          </tbody>
          </table>     
          <div className='pagination'>
            <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={Math.ceil(events.length / itemsPerPage)}
        
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageChange}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
      /></div>
             
          </div>

        </div>




          {/*invitations*/}

        
    </div> 
    </div>
  )
};

export default Campstab;
