import React, { useState, useEffect } from "react";
import DarkTheme from "../../layouts/Dark";
import Link from "next/link";
import Head from 'next/head';
import { Dropdown, Avatar, Text, Grid, User } from "@nextui-org/react";
import { createTheme, NextUIProvider } from "@nextui-org/react";
import { Notification } from 'react-iconly';
import Router from 'next/router';
import { Bell , MessageSquare} from "@geist-ui/react-icons";
const theme = createTheme({
  type: "dark", // it could be "light" or "dark"
  theme: {
    colors: {
      // brand colors
      primaryLight: '$green200',
      primaryLightHover: '$green300',
      primaryLightActive: '$green400',
      primaryLightContrast: '$green600',
      primary: '#4ADE7B',
      primaryBorder: '$green500',
      primaryBorderHover: '$green600',
      primarySolidHover: '$green700',
      primarySolidContrast: '$white',
      primaryShadow: '$green500',

      gradient: 'linear-gradient(112deg, $blue100 -25%, $pink500 -10%, $purple500 80%)',
      link: '#5E1DAD',

      // you can also create your own color
      myColor: '#ff4ecd'

      // ...  more colors
    },
    space: {},
    fonts: {}
  }
});

const Sidebar = ({ socket }) => {


  console.log(socket)

  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    //socket = io(ENDPOINT);
    socket.on("getNotification", (data) => {
      console.log(data)
      setNotifications((prev) => [...prev, data]);
    });
    return () => {
      socket.off("getNotification");
    };
  }, [socket]);

  console.log(notifications);
  
  let name = '';
  let lastname = '';
  let role = '';
  let email = '';

  if (typeof window !== 'undefined') {
    const userData = localStorage.getItem('userData');
    const parsedUserData = JSON.parse(userData);
    name = parsedUserData.name;
    lastname = parsedUserData.lastname;
    role = parsedUserData.role;
    email = parsedUserData.email;
  }
  const logout = () => {
    localStorage.removeItem('userData');
    // Redirect to login page
    Router.push('/blog-list/blog-list-dark/');
  };
  return (

    <div>
      <Head>
      <link rel="stylesheet" href="/css/ionicons.min.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet" />
        {/* Icon Font Stylesheet */}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/bootstrap v5.css" />
        <link rel="stylesheet" href="../../css/style.css" />
        {/* Libraries Stylesheet */}
        <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
        <link href="../lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
      </Head>
      <DarkTheme>

        {/* Sidebar Start */}
        <div className="sidebar bg pe-4 pb-3 width-10">
          <nav className="navbar bg navbar-dark">
            <a href="/" className="navbar-brand mt-3 mx-4 mb-4">
              <h3 className="text-u ls5 back-color">InfluenX</h3>
            </a>
            <div className="d-flex align-items-center ms-4 mb-4">
              <div className="position-relative">
                <img className="rounded-circle" src="../img/clients/emna.jpg"  style={{ width: 40, height: 40 }} />
                <div className="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1" />
              </div>
              <div className="ms-3">
                <h6 className="mb-0 color-font">{name} {lastname}</h6>
                <span>{role}</span>
              </div>
            </div>
            <div className="navbar-nav w-100">
              <a href="/Annonceur/dashboard" className="fz-16 ml-2 nav-item nav-link  ">                  <i className="fa fa-tachometer-alt me-2" />Dashboard</a>
              <a href="/Annonceur/campagnes" className="fz-16 ml-2 nav-item nav-link  ">                        <i className="fa fa-bullhorn me-2" />Campagnes</a>
              <a href="/Annonceur/collaborations" className="fz-16 ml-2 nav-item nav-link  ">                        <i className="fa fa-handshake me-2" />collaborations</a>

              <a href="/Annonceur/reseaux_sociaux" className="fz-16 ml-2 nav-item nav-link">                    <i className="fa fa-icons me-2" />Réseaux Sociaux</a>
              <a href={`/Annonceur/evenements`} className="fz-16 ml-2 nav-item nav-link active ">                       <i className="fa fa-calendar me-2" />Évènements</a>
              <a href="/Annonceur/recherche" className="fz-16 ml-2 nav-item nav-link  ">                        <i className="fa fa-search me-2" />Recherche</a>
              <a href="/Annonceur/boite_de_reception" className="fz-16 ml-2 nav-item nav-link  ">               <i className="fa fa-inbox me-2" />Boîte de réception</a>
              

            </div>
          </nav>
        </div>
        {/* Sidebar End */}

        {/* Content Start */}
        <div className="container">

          {/* Navbar Start */}


          <nav className="navbar navbar-expand-lg nav-link bg  sticky-top px-4 py-0">
           

            <div className="navbar-nav align-items-center ms-auto">
              <Grid.Container gap={1.5} justify="flex-start">
                <Grid xs={12}>
                  <Grid>
                    <Dropdown>
                      <Dropdown.Button color='primary'>
                        <MessageSquare size={18} />  Messages
                      </Dropdown.Button>
                      <Dropdown.Menu
                        color='black'
                        variant="black"
                        aria-label="Actions"
                      >
                        <Dropdown.Item key="new" color="primary" >Messages1</Dropdown.Item>
                        <Dropdown.Item key="copy" color="primary">Messages2</Dropdown.Item>
                        <Dropdown.Item key="edit" color="primary">Messages3</Dropdown.Item>

                      </Dropdown.Menu>
                    </Dropdown>
                  </Grid>
                </Grid>

              </Grid.Container>

              <Grid.Container gap={1.5} justify="flex-start">
                <Grid xs={12}>
                  <Grid>
                    <Dropdown>
                      <Dropdown.Button color="secondary">
                        <Bell size={18} /> Notifications
                      </Dropdown.Button>
                      <Dropdown.Menu color="black" variant="black" aria-label="Actions">
                        {notifications.map((notification, index) => (
                          <Dropdown.Item key={index} color="secondary">
                           crée un événement {notification.senderTitle} dans le domaine: {notification.senderDomain}
                          </Dropdown.Item>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Grid>
                </Grid>
              </Grid.Container>
              <Grid.Container justify="flex-start" gap={1}>
    <Grid>
      <button onClick={logout}>Déconnexion</button>
    </Grid>
  </Grid.Container>

              <div className='ms-3'>
                <Grid.Container justify="flex-start" gap={1}>
                  <Grid>
                    <Dropdown placement="bottom-left">
                      <Dropdown.Trigger>
                        <User
                          bordered
                          as="button"
                          size="lg"
                          color="#fff"
                          name={<span
                            style={{
                              background: "linear-gradient(to right, #12c2e9, #c471ed,#f64f59)",
                              "-webkit-background-clip": "text",
                              "-webkit-text-fill-color": "transparent",
                            }}
                          >
                            {name} {lastname}
                          </span>}
                          description={<span style={{ color: "#fff" }}>{role}</span>}
                          src="https://i.pravatar.cc/150?u=a042581f4e29026024d"

                        />
                      </Dropdown.Trigger>
                      <Dropdown.Menu color="primary" aria-label="User Actions">
                        <Dropdown.Item key="profile" css={{ height: "$18" }}>
                          <Text b color="inherit" css={{ d: "flex" }}>
                           connectée par
                          </Text>
                          <Text b color="inherit" css={{ d: "flex" }}>
                            {email}
                          </Text>
                        </Dropdown.Item>

                        <Dropdown.Item key="team_settings"><a href="/Annonceur/modifierprofil/">Modifier profil</a></Dropdown.Item>
                        
                        <Dropdown.Item key="settings" withDivider><a href="/Annonceur/modifiermotdepasse/">
Modifier mot de passe                        </a></Dropdown.Item>


                        <Dropdown.Item key="analytics" withDivider><a href="/Annonceur/modifierreseauxsociaux/">
                          Modifier réseaux sociaux
                        </a></Dropdown.Item>

                        <Dropdown.Item key="logout" color="error" withDivider>
                          Déconnexion
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Grid>
                </Grid.Container>
              </div>

            </div>

          </nav>

          {/* Navbar End*/}

        </div>


      </DarkTheme>

    </div>

  )
};

export default Sidebar;
