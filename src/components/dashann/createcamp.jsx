import React, { useState, useEffect } from "react";
import { Formik, Form, Field, setFieldValue } from "formik";
import Select from "react-select";
import { FaFacebook, FaTwitter, FaLinkedin } from 'react-icons/fa';
import { faFlag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import api from "../../utils/axiosInstance";
import MultiSelect from "../MultiSelect/MultiSelect";


const ENDPOINT = "http://localhost:3001";

const Createcamp = ({ socket }) => {
  const [file, setFile] = useState()
  console.log(socket)
  let showNotification = false;
  const handleNotification = (e) => {
    e.preventDefault();
    // Emit a "cree event" message
    // socket.emit("cree event", "cree event dans le domaine zzz");

    socket.emit("sendsetNotification", {
      senderName: namecamp.value,
      senderDomain: domaincamp.value,

    });
    console.log(namecamp.value);
    console.log(domaincamp.value);
  };
  //showNotification = true;



  const [message, setMessage] = useState("");
  const handleSubmit = async (values) => {
    console.log("values  : ", values);
    const formData = new FormData()
    Object.entries(values).forEach(([key,value])=>{
      formData.append(key,value)
    })
    formData.append('file',file)
    console.log(formData)
    try {

      const response = await api.post("campagnes", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        }});
      socket.emit("sendsetNotification", {
        senderName: values.namecamp,
        senderDomain: values.domaincamp,

      });
      console.log(response.data);
      setMessage("Compagne créé avec succès");
    } catch (error) {
      console.log(error);
    }
  };

  const messageRef = React.useRef(null);
  const sendMessage = (ms) => new Promise((r) => setTimeout(r, ms));

  //domaines
  const [languages, setLanguages] = useState([]);

  const handleLanguageInputChange = (event) => {
    setLanguages(event.target.value)
    // const values = event.target.value.split(","); // sépare les choix entrés par des virgules
    // setLanguages(values.map((value) => value.trim())); // supprime les espaces avant et après chaque choix
  };

  //réseaux sociaux
  const [selectedSocialNetworks, setSelectedSocialNetworks] = useState([]);
  const options = [
    { value: 'facebook', label: 'Facebook', icon: <FaFacebook /> },
    { value: 'twitter', label: 'Twitter', icon: <FaTwitter /> },
    { value: 'linkedin', label: 'LinkedIn', icon: <FaLinkedin /> },
  ];
  const handleSocialNetworksChange = (selectedOptions) => {
    let reseausoc = [];
    if (selectedOptions.length > 0) {
      selectedOptions.forEach(element => reseausoc.push(element.value));
    }
    if (reseausoc.length > 0) {
      console.log("reseau", reseausoc);
      setSelectedSocialNetworks(reseausoc);
    }
  };
  const getValue = () => {
    if (options) {
      return isMulti
        ? options.filter((option) => field.value.indexOf(option.value) >= 0)
        : options.find((option) => option.value === field.value);
    } else {
      return isMulti ? [] : ('');
    }
  };
  console.log(options.filter((option) => selectedSocialNetworks.includes(option.value)))
  //pays
  const countries = [
    { value: 'US', label: 'United States', icon: faFlag },
    { value: 'CA', label: 'Canada', icon: faFlag },
    { value: 'MX', label: 'Mexico', icon: faFlag },
  ];
  const [selectedCountries, setSelectedCountries] = useState([]);

  const handleCountriesChange = (selectedOptions) => {
    const selectedValues = Array.isArray(selectedOptions)
      ? selectedOptions.map((option) => option.value)
      : [];

    setSelectedCountries(selectedValues);
  };
  return (
    <div>

      <section className="contact section-padding" style={{
        width: '950px',
        paddingRight: '100px',
        paddingLeft: '10px',
        marginTop: '100px',
        marginRight: '100px',
        marginLeft: '335px',
      }} >
        <div className="container"
          style={{
            width: '1180px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-80px',
            marginRight: '100px',
            marginLeft: '10px',
          }}>
          <div className="row bg1 rounded text-center p-4">
            <div className="col-lg-6">
              <div className="form md-mb50" style={{
                width: '1000px',
                paddingRight: '100px',
                paddingLeft: '10px',
                marginTop: '40px',
                marginRight: '100px',
                marginLeft: '30px',
              }}>
                <h3 className="color-font "
                  style={{
                    width: '1100px',
                    paddingRight: '100px',
                    paddingLeft: '10px',
                    marginTop: '-40px',
                    marginBottom: '50px',
                    marginRight: '100px',
                    marginLeft: '-320px',
                  }} >Créer une campagne</h3>
                <Formik
                  initialValues={{
                    namecamp: "",
                    descriptioncamp: "",
                    domaincamp: "",
                    countrycamp: [],
                    photo: "",
                    nbfollower: "",
                    reseausoc: [],
                  }}
                  onSubmit={handleSubmit}
                >
                  {({ isSubmitting }) => (
                    <Form id="contact-form">
                      <div className="messages">
                        {message && <p>{message}</p>}
                      </div>
                      <div className="controls">
                        <div className="form-group">
                          <label htmlFor="form_photo">Nom de campagne :</label>
                          <Field
                            id="namecamp"
                            type="text"
                            name="namecamp"
                            placeholder="Nom de campagne"
                            required="required"
                          />
                        </div>


                      </div>
                      <div className="form-group">
                        <label htmlFor="form_description">Description de campagne :</label>
                        <Field
                          as="textarea"
                          id="descriptioncamp"
                          name="descriptioncamp"
                          placeholder="description de campagne"
                          rows="6"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="form_photo">Photo du produit :</label>
                        <input
                          type="file"
                          id="photo"
                          name="photo"
                          onChange={(event) => {
                            setFile(event.target.files[0]);
                          }}
                        />
                      </div>




                      <div className="form-group">
                        <label htmlFor="form_domain">Domaines :</label>
                        <Field
                          type="text"
                          name="domaincamp"
                          id="domaincamp"
                          placeholder="Entrer domaines séparés par des virgules"
                          required="required"
                        />
                      </div>



                      <label htmlFor="socialNetworks">Réseaux sociaux</label>
                      <div  >
                        {/* <Select className="form-select w-100 mb-4"
                        id="reseausoc"
                        name="reseausoc"
                        options={options}

                        isMulti={true}
                        onChange={handleSocialNetworksChange}
                        getOptionLabel={(option) => (
                          <>
                            {option.icon} {option.label}
                          </>
                        )}
                        getOptionValue={(option) => option.value}
                      /> */}
                        <Field
                          id="reseausoc"
                          name="reseausoc"
                          placeholder="Multi Select"
                          isMulti={true}
                          component={MultiSelect}
                          options={options}
                        />

                      </div>
                      <div className="form-group">
                        <label htmlFor="form_followers">Nombre d'abonnés voulus :</label>
                        <Field
                          id="nbfollower"
                          type="text"
                          name="nbfollower"
                          placeholder="Nombre d'abonnés voulus"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="country">Pays</label>
                        {/* <Select className="form-select w-100 mb-4"
                        id="countrycamp"
                        name="countrycamp"
                        options={countries}
                        isMulti
                        onChange={handleCountriesChange}
                        value={selectedCountries.map((value) =>
                          countries.find((option) => option.value === value)
                        )}
                        getOptionLabel={(option) => (
                          <>
                            <FontAwesomeIcon icon={option.icon} />
                            <span>{option.label}</span>
                          </>
                        )}
                        getOptionValue={(option) => option.value}
                      /> */}
                        <Field
                          id="countrycamp"
                          name="countrycamp"
                          placeholder="Multi Select"
                          isMulti={true}
                          component={MultiSelect}
                          options={countries}
                        />
                      </div>

                      <button type="submit" disabled={isSubmitting} className="butn bord">
                        <span>Enregistrer</span>
                      </button>
                    </Form>
                  )}
                </Formik>
              </div>
            </div>


          </div>
        </div>


      </section>
    </div>
  );
};

export default Createcamp;
