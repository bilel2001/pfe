import React from 'react'
import ReactPaginate from 'react-paginate';
import { useState } from 'react';
import api from '../../utils/axiosInstance';
import { useRef } from 'react';



const Colltab = () => {

  const [campagnes, setCampagnes] = useState([]);
  const localStorageRef = useRef()
  const [newCampaign, setNewCampaign] = useState("");

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`users/status/users/accepter`);
        const data = response.data;
        setCampagnes(data);
        console.log(data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData()
  }, []);

  React.useEffect(() => {
    localStorageRef.current = JSON.parse(localStorage.getItem("userData"))
  }, []);




  const [currentPage, setCurrentPage] = useState(0);
  const perPage = 5;
  const [users, setUsers] = useState([
    { id: 1, name: "influenceur 1", campaign: "bla bla", status: "activé" },
    { id: 2, name: "influenceur 2", campaign: "bla bla", status: "en cours" },
    { id: 3, name: "influenceur 3", campaign: "bla bla", status: "terminé" },
    { id: 4, name: "influenceur 4", campaign: "bla bla", status: "annulé" },
  ]);

  // const [newCampaign, setNewCampaign] = useState("");


  const handleStatusChange = (e, index) => {
    const { value } = e.target;
    const newUsers = [...users];
    newUsers[index].status = value;
    setUsers(newUsers);
  };


  // const handleAddCampaign = () => {
  //   const newId = users.length + 1;
  //   const newCampaignObj = { id: newId, name: newCampaign, status: "activé" };
  //   setUsers([...users, newCampaignObj]);
  //   setNewCampaign("");
  // };


  const handleDelete = (index) => {
    const newUsers = [...users];
    newUsers.splice(index, 1);
    setUsers(newUsers);
  };
  const pages = users.slice(currentPage * perPage, (currentPage + 1) * perPage);

  const handlePageClick = (data) => {
    setCurrentPage(data.selected);
  };


  

  return (

    <div>

      <div style={{
        width: '1300px',
        paddingRight: '100px',
        paddingLeft: '10px',
        marginTop: '100px',
        marginRight: '100px',
        marginLeft: '300px',
      }}>

        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '40px',
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-290px',
          }}>Table des collaborations</h3>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="text-center color-font">

                  <th scope="col">Influenceurs</th>
                
               
                  <th scope="col">Action</th>

                </tr>
              </thead>
              <tbody>
              {campagnes
                  .filter((campagne) =>
                    campagne.collaborateurs.find(p => p?.demendeur?._id === localStorageRef.current?._id)
                  )
                  .map(camp => (

                    
                  <tr key={camp._id} className="text-white text-center">

                    <td>{camp.name} {camp.lastname}  </td>
                    
                   
                    <td className=" d-flex justify-content-center align-items-center">
                      <button onClick={() => handleDelete(index)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                     
                    </td>

                  </tr>

                ))}
              </tbody>
            </table>


            {/* pagination */}
            <div className='pagination'>
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                pageCount={Math.ceil(users.length / perPage)}
                onPageChange={handlePageClick}
                containerClassName={'pagination justify-content-center'}
                activeClassName={'active'}
              />
            </div>
          </div>
        </div>


        






      </div>



    </div>

  )
};

export default Colltab;



















{/* <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '40px',
          marginBottom: '20px',
        }}>
</div> */}
{/* <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-310px',
          }}>Créer collaboration</h3> */}

{/* <div className="container-fluid pt-4 px-4" > */ }

{/* <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Collaboration"
                value={newCampaign}
                onChange={(e) => setNewCampaign(e.target.value)}
              />
              <button
                className="btn btn-primary"
                type="button"
                onClick={handleAddCampaign}
              >
                Créer collaboration
              </button>
              
            </div> */}
{/* </div> */ }
{/* demandes */ }











