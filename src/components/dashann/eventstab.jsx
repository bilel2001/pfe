import React from 'react'
import ReactPaginate from 'react-paginate';
import Link from "next/link";
import { useState } from 'react'; 
import api from "../../utils/axiosInstance";




  
const Eventstab = () => {
  const [invitations, setInvitations] = useState([]);
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`events`);
        const data = response.data;
        console.log(data);
        setInvitations(data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData(); 

  }, []);


  const handleDelete = async (e, eventId) => {
    e.preventDefault()
    await api.delete(`events/${eventId}/delete`)
  }

  const events  = [

    {id: '1' ,title: 'NMTCv1', description:'National Microsoft Technologies Camp est le plus grand événement informatique du pays et cest une collaboration entre tous les clubs Microsoft en Tunisie.', organizer:'Mtc clubs', domains:'Digital', startdate:'23/03/2020' , enddate:'25/03/2020' , place:'Sousse'   },
    {id: '2' ,title: 'NMTCv2', description:'National Microsoft Technologies Camp est le plus grand événement informatique du pays et cest une collaboration entre tous les clubs Microsoft en Tunisie.', organizer:'Mtc isims', domains:'Digital', startdate:'23/03/2021' , enddate:'25/03/2021' , place:'Hammamet'     },
    {id: '3' ,title: 'NMTCv3', description:'National Microsoft Technologies Camp est le plus grand événement informatique du pays et cest une collaboration entre tous les clubs Microsoft en Tunisie.', organizer:'Mtc isims', domains:'Digital', startdate:'23/03/2022' , enddate:'24/03/2022' , place:'Tunis'    },
  ];
  const [currentPage, setCurrentPage] = useState(0);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };
  const itemsPerPage = 3;
  
  const eventsToDisplay = events.slice(
    currentPage * itemsPerPage,
    (currentPage + 1) * itemsPerPage
  );

 

 

  
  
    
  return (
<div>
    <div className="container-fluid pt-4 px-4"
    style={{
      width: '1100px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '50px',
      marginRight: '100px',
      marginLeft: '350px',
    }}
  >

{/*tableau*/}
<div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '-100px',
          marginBottom: '100px',
          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <h3 className="color-font" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
         marginTop: '-10px',
         marginBottom: '10px',
          marginRight: '100px',
          marginLeft: '-315px',
        }}>Mes évènements</h3>
          <div className="table-responsive">
          <table className="table text-start align-middle table-bordered table-hover mb-0">
          <thead>
                <tr className="text-center color-font ">

                  <th scope="col">Titre</th>
                  
                  <th scope="col">Description</th>
                  <th scope="col">Organizateur</th>
                  <th scope="col">Domaines</th>
                  <th scope="col">Date début</th>
                  <th scope="col">Date fin</th>
                  <th scope="col">Lieu</th>
                  <th scope="col">Action</th>


                  

                </tr>
                </thead>
                <tbody>
                {invitations.map((event, index) => (
            
              <tr key={event.id} className="text-white">
                
                 <td>{event.title}          </td>
                 <td>{event.Descriptionevent}        </td>
                 <td>{event.organiseurevent}        </td>
                 <td>{event.domainevent}        </td>
                 <td>{event.dated}        </td>
                 <td>{event.datef}        </td>
                 <td>{event.lieu}        </td>

                 <td className=" grid">
                      <button onClick={(e) => handleDelete(e, event?._id)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                      <button onClick={() => handleDelete(index)} className="btn btn-info" style={{marginTop:'10px',
                    width:'128px'}}> <i className="fas fa-edit"></i> Modifier</button>
                    </td>
                 </tr>
                
          ))} 
          </tbody>
          </table>     
          <div className='pagination'>
            <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={Math.ceil(events.length / itemsPerPage)}
        
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageChange}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
      /></div>
             
          </div>

        </div>




          {/*invitations*/}

        
    </div> 
    </div>
  )
};

export default Eventstab;
