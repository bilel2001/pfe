import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import api from "../../utils/axiosInstance";

const invitationsData = [

  { id: '1', nom: 'emna', domaine: 'digital' },
  { id: '2', nom: 'bilel', domaine: 'digital' },
  { id: '3', nom: 'imen', domaine: 'digital' },
  { id: '4', nom: 'ahmed', domaine: 'digital' },
  { id: '5', nom: 'abir', domaine: 'digital' },
  { id: '6', nom: 'flenfouleni', domaine: 'digital' }
];
const Eventtab = ({ eventId }) => {

  const [invitations, setInvitations] = useState([]);
  const [tableAccepted, setTableAccepted] = useState([]);
  const [acceptations, setAcceptations] = useState([]);
  const [success, setSuccess] = useState(false);


  const [currentPage, setCurrentPage] = useState(0);
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`events/status/users`);
        const data = response.data;
        setInvitations(data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData().then(() => setSuccess(false));
  }, [success]);






  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };





  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(`events/status/users/accepter`);
        const data = response.data;
        setAcceptations(data);

        console.log(data)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData().then(() => setSuccess(false))
  }, [success]);
  const handleDelete = async (e, demendeId,eventId) => {
    e.preventDefault()
    await api.delete(`events/delete/${demendeId}/${eventId}`)
  }

  const invitationsPerPage = 2;
  const startIndex = currentPage * invitationsPerPage;
  const endIndex = startIndex + invitationsPerPage;
  const invitationsToDisplay = tableAccepted.slice(startIndex, endIndex);

  const acceptInvitation = async (e, invitation, eventId) => {
    e.preventDefault()
    await api.put(`events/status/${invitation.demendeur._id}/${eventId}`).then(() => setSuccess(true))
  }

  function rejectInvitation(invitation) {
    setInvitations((prevInvitations) => prevInvitations.filter((prevInvitation) => prevInvitation !== invitation));
    setSuccess(true)

  }


  return (
    <div>
      <div className="container-fluid pt-4 px-4"
        style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '50px',
          marginRight: '100px',
          marginLeft: '350px',
        }}
      >

        {/*tableau*/}
        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '0px',
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-315px',
          }}>Demandes acceptées</h3>
          <div className="table-responsive">
            <table className="table text-start align-middle table-bordered table-hover mb-0">
              <thead>
                <tr className="color-font text-center">

                  <th scope="col">Participants</th>

                  <th scope="col">évènement</th>
                  <th scope="col">Action</th>

                </tr>
              </thead>
              <tbody>
                {acceptations.map((acceptations, index) => (
                  acceptations.partcants.map(part => (

                    <tr key={part._id} className='text-white'>

                      <td>{part.demendeur?.name}         </td>
                      <td>{acceptations?.title}       </td>
                      <td >
                        <button onClick={(e) => handleDelete(e, part?._id,acceptations?._id)} className="btn btn-danger"> <i className="fas fa-trash-alt"></i> Supprimer</button>
                        
                      </td>
                    </tr>

                  ))

                ))}
              </tbody>
            </table>
            <div className='pagination'>
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                pageCount={Math.ceil(tableAccepted.length / invitationsPerPage)}

                onPageChange={handlePageChange}
                containerClassName={'pagination justify-content-center'}
                activeClassName={'active'}
              /></div>

          </div>

        </div>




        {/*invitations*/}

        <div className="bg1 text-center rounded p-4" style={{
          width: '1100px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '40px',
          marginRight: '100px',
          marginLeft: '0px',
          marginBottom: '100px'
        }}>
          <h3 className="color-font" style={{
            width: '1100px',
            paddingRight: '100px',
            paddingLeft: '10px',
            marginTop: '-10px',
            marginBottom: '10px',
            marginRight: '100px',
            marginLeft: '-250px',
          }}>
            Demandes de participation
          </h3>

          <div className="row g-4">
            <div className="col-sm-12">
              <div className="h-100 bg1 rounded p-4">
                {invitations.map((invitation, index) => (

                  invitation.partcants
                    .filter(part => part.status !== 'accepter')
                    .map(part => (
                      <div key={part._id} className="d-flex align-items-center border-bottom py-3">
                        <img className="rounded-circle flex-shrink-0" src="img/user.jpg" alt style={{ width: 40, height: 40 }} />
                        <div className="w-100 ms-3">
                          <div className="d-flex w-100 justify-content-between">
                            <h6 className="mb-0">{part.demendeur?.name}</h6>



                            <h6 className="mb-0">{part.demendeur?.domain}</h6>

                          </div>

                        </div>
                        <div className="d-flex align-items-center justify-content-center ms-auto ml-5">
                          <button className='btn'>
                          <i className="bi bi-eye-fill text-primary" style={{ fontSize: 20 }} >  Consulter</i></button>
                          <button className='btn'
                            onClick={(e) => acceptInvitation(e, part, invitation._id)}
                          >
                            <i className="bi bi-check-circle-fill text-success " style={{ fontSize: 20 }} > Accepter</i>

                          </button>
                          <button className='btn'
                          onClick={(e) => rejectInvitation(e, part, invitation._id)}>
                          <i className="bi bi-x-circle-fill text-danger" style={{ fontSize: 20 }} > Refuser</i>
                          </button>
                        </div>
                      </div>
                    ))

                ))}


              </div>
            </div>

          </div>

        </div>

      </div>
    </div>
  )
};

export default Eventtab;
