import React, { useState, useEffect } from "react";
import { Formik, Form, Field, setFieldValue } from "formik";
import axios from "axios";
import { useHistory } from "react-router-dom";
import api from "../../utils/axiosInstance";

const ENDPOINT = "http://localhost:3001";
const Eventform = ({ socket }) => {
  const [file, setFile] = useState()
  console.log(socket)
  let showNotification = false;
  const handleNotification = (e) => {
    e.preventDefault();
    // Emit a "cree event" message
    // socket.emit("cree event", "cree event dans le domaine zzz");

    socket.emit("sendsetNotification", {
      senderTitle: title.value,
      senderDomain: domainevent.value,

    });
    console.log(title.value);
    console.log(domainevent.value);
  };
  //showNotification = true;



  const [message, setMessage] = useState("");
  const handleSubmit = async (values) => {
    console.log("values  : ", values);
    const formData = new FormData()
    Object.entries(values).forEach(([key,value])=>{
      formData.append(key,value)
    })
    formData.append('file',file)
    console.log(formData)
    try {
      //const instance = axios.create({ baseURL: 'http://localhost:3001' })
      const response = await api.post("events", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        }});
      socket.emit("sendsetNotification", {
        senderTitle: values.title,
        senderDomain: values.domainevent,
        

      });
      
      console.log(response.data);
      // Store event data in local storage
    localStorage.setItem('eventData', JSON.stringify(response.data));
    console.log(localStorage)
      setMessage("Event créé avec succès");
    } catch (error) {
      console.log(error);
    }
  };

  const messageRef = React.useRef(null);
  const sendMessage = (ms) => new Promise((r) => setTimeout(r, ms));

  //domaines
  const [languages, setLanguages] = useState([]);

  const handleLanguageInputChange = (event) => {
    const values = event.target.value.split(","); // sépare les choix entrés par des virgules
    setLanguages(values.map((value) => value.trim())); // supprime les espaces avant et après chaque choix
  };







  return (
    <section className="contact section-padding" style={{
      width: '950px',
      paddingRight: '100px',
      paddingLeft: '10px',
      marginTop: '-200px',
      marginRight: '100px',
      marginLeft: '335px',
    }} >
      <div className="container"
        style={{
          width: '1180px',
          paddingRight: '100px',
          paddingLeft: '10px',
          marginTop: '10px',
          marginRight: '100px',
          marginLeft: '35px',
        }}>
        <div className="row bg1 rounded text-center p-4">
          <div className="col-lg-6">
            <div className="form md-mb50" style={{
              width: '1000px',
              paddingRight: '100px',
              paddingLeft: '10px',
              marginTop: '40px',
              marginRight: '100px',
              marginLeft: '30px',
            }}>
              <h3 className="color-font "
                style={{
                  width: '1100px',
                  paddingRight: '100px',
                  paddingLeft: '10px',
                  marginTop: '-40px',
                  marginBottom: '50px',
                  marginRight: '100px',
                  marginLeft: '-320px',
                }} >Créer un évènement</h3>


              <Formik
                initialValues={{
                  title: "",
                  organiseurevent: "",
                  dated: "",
                  datef: "",
                  lieu: "",
                  Descriptionevent: "",
                  photoevent: "",
                  domainevent: "",
                }}
                onSubmit={handleSubmit}
              >
                {({ isSubmitting, setFieldValue }) => (
                  <Form id="contact-form">
                   <div className="messages">
                      {message && <p>{message}</p>}
                    </div>
                    <div className="controls">
                      <div className="form-group">
                        <label htmlFor="form_photo">Titre :</label>
                        <Field
                          id="title"
                          type="text"
                          name="title"
                          placeholder="Titre d'évènement"
                          required="required"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="form_photo">Organisateur :</label>
                        <Field
                          id="organiseurevent"
                          type="text"
                          name="organiseurevent"
                          placeholder="Organizateur"
                          required="required"
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="form_photo">Date debut :</label>
                        <Field
                          id="dated"
                          type="date"
                          name="dated"
                          placeholder="Date de naissance"
                          required="required"
                          onChange={(event) =>
                            setFieldValue("dated", event.target.value) // Use setFieldValue to set the value of the field
                          }

                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="form_photo">Date fin :</label>
                        <Field
                          id="datef"
                          type="date"
                          name="datef"
                          placeholder="Date de naissance"
                          required="required"
                          onChange={(event) =>
                            setFieldValue("datef", event.target.value) // Use setFieldValue to set the value of the field
                          }

                        />
                      </div>

                    </div>
                    <div className="form-group">
                      <label htmlFor="form_photo">Lieu :</label>
                      <Field
                        id="lieu"
                        type="text"
                        name="lieu"
                        placeholder="Lieu d'évènement"
                        required="required"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="form_description">Description :</label>
                      <Field
                        as="textarea"
                        id="Descriptionevent"
                        name="Descriptionevent"
                        placeholder="description d'évènement"
                        rows="6"
                        required="required"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="form_photo">Photo d'évènement :</label>
                      <input
                        type="file"
                        id="photoevent"
                        name="photoevent"
                        onChange={(event) => {
                          setFile(event.target.files[0]);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="form_domain">Domaines :</label>
                      <Field
                        type="text"
                        name="domainevent"
                        id="domainevent"
                        placeholder="Entrer domaines séparés par des virgules"
                        required="required"
                      />
                    </div>
                    <button type="submit" disabled={isSubmitting} className="butn bord">
                      <span>Enregistrer</span>
                    </button>
                  </Form>
                )}
              </Formik>
            </div>
          </div>


        </div>
      </div>


    </section>
  );
};

export default Eventform;