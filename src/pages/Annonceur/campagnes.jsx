import { useEffect, useState } from "react";
import DarkTheme from "../../layouts/Dark";


import Sidebar from "../../components/dashann/sidecamp";

import Createcamp from "../../components/dashann/createcamp"
import Camptab from "../../components/dashann/camptab";

import socketIO from "socket.io-client";
const socket = socketIO.connect("http://localhost:3001");
import Campstab from "../../components/dashann/campstab";


const Dashboard = () => {
  // const [socket, setSocket] = useState(null);
  // useEffect(() => {
  //   setSocket(io("http://localhost:3001"));
  // }, []);
  return (
    <DarkTheme>
      <Sidebar socket={socket}/>
      <Camptab/>
      <Campstab/>
      <Createcamp  socket={socket}/>
      
     

    </DarkTheme>
  );
};

export default Dashboard;
