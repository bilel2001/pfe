import React from "react";

import DarkTheme from "../../layouts/Dark";

import LeftNavbar from "../../components/Admin/LeftNavbar";
import Header from "../../components/Admin/Header";
import Tableau from "../../components/Admin/Tableau";
import Sidebar from "../../components/dashann/sidemodmdp";
import Widgers from "../../components/Admin/Widgers";
import Sale from "../../components/Admin/Sale";
import  Chart  from "../../components/Admin/Chart";
import paramform from "../../components/dashinf/paramform"
import ContactForm from "../../components/Contact-form/contact-form";
import Editmdp from "../../components/dashann/editmdp";







const Dashboard = () => {
 
  return (
    <DarkTheme>
  <Sidebar/>
  <Editmdp />
  
    </DarkTheme>
  );
};

export default Dashboard;
