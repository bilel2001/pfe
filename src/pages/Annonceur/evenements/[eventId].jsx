import React from "react";

import DarkTheme from "../../layouts/Dark";


import Sidebar from "../../components/dashann/sideev";
import Eventtab from "../../components/dashann/eventtab"
import Eventform from "../../components/dashann/eventform"
import Eventstab from "../../components/dashann/eventstab"

import socketIO from "socket.io-client";
import { useRouter } from "next/router";
const socket = socketIO.connect("http://localhost:3001");




const Dashboard = () => {
    const router = useRouter()

    const {eventId} = router.query
  return (
    <DarkTheme>
  <Sidebar socket={socket}/>
  <Eventtab eventId={eventId}/>
  <Eventform socket={socket}/>
    </DarkTheme>
  );
};

export default Dashboard;
