import React from "react";

import DarkTheme from "../../layouts/Dark";


import Sidebar from "../../components/dashann/sideev";
import Eventtab from "../../components/dashann/eventtab"
import Eventform from "../../components/dashann/eventform"
import Eventstab from "../../components/dashann/eventstab"

import socketIO from "socket.io-client";
const socket = socketIO.connect("http://localhost:3001");




const Dashboard = () => {

  return (
    <DarkTheme>
      <Sidebar socket={socket} />
      
      <Eventtab />
      <Eventstab/>
      <Eventform socket={socket} />
    </DarkTheme>
  );
};

export default Dashboard;
