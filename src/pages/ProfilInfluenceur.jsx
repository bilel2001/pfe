/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import Typewriter from "typewriter-effect";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { faCamera } from "@fortawesome/free-solid-svg-icons";

const ProfilInfluenceur = () => {
  const [image, setImage] = useState("");

  const handleImageUpload = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      setImage(e.target.result);
    };
  };

  return (
    <header className="freelancre valign">
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <div className="img">
              <img src={image || "/img/hero.jpg"} alt="" />
              <div
                className="edit-photo-icon"
                style={{
                  position: "absolute",
                  bottom: "0",
                  right: "0",
                  backgroundColor: "#fff",
                  color: "#333",
                  borderRadius: "20%",
                  padding: "5px",
                  cursor: "pointer",
                }}
              >
                <label htmlFor="imageUpload">
                  <FontAwesomeIcon icon={faCamera} />
                </label>
                <input
                  type="file"
                  id="imageUpload"
                  hidden
                  onChange={handleImageUpload}
                />
              </div>
            </div>
          </div>
          <div className="col-lg-8 valign">
            <div className="cont">
              <div className="notification-icon">
                <FontAwesomeIcon icon={faBell} />
              </div>
              <h1 className="cd-headline clip">
                Hello, My name is hisham i love design and i hope to make
                awesome designs and also i create a
                <span
                  style={{ fontSize: "35px", lineHeight: "49px" }}
                  className="cd-words-wrapper"
                >
                  <Typewriter
                    options={{
                      wrapperClassName: "color-font fw-600",
                      strings: [
                        "Mobile Apps",
                        "Landing Pages",
                        "Awesome Design",
                      ],
                      autoStart: true,
                      loop: true,
                    }}
                    loop={true}
                    onInit={(typewriter) => {
                      typewriter;
                    }}
                  />
                </span>
              </h1>
              <div className="mt-3">
                <a href="#" className="badge badge-light mr-1">Fitness</a> <a href="#" className="badge badge-light mr-1">Life Style</a> <a href="#" className="badge badge-light">Gym</a>
              </div>
              <div className="border-top user-social-box">
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 twitter-color"> <i className="fab fa-twitter-square" /></span ><span style={{ marginRight: '20px' }}>13,291</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 tiktok-color"><i className="fab fa-tiktok"></i></span><span style={{ marginRight: '20px' }}>84,019</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 instagram-color"> <i className="fab fa-instagram" /></span><span style={{ marginRight: '20px' }}>12,300</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2  facebook-color"> <i className="fab fa-facebook-square " /></span><span style={{ marginRight: '20px' }}>92,920</span></div>
                <div className="user-social-media d-xl-inline-block"><span className="mr-2 youtube-color"> <i className="fab fa-youtube" /></span><span style={{ marginRight: '20px' }}>1291</span></div>
                <Link href={`/contact/contact-dark/`}>
                  <div className="container">
                    <div className="col-lg-8 offset-lg-9">
                      <a className="butn bord curve mt-30"> <span>Modifier le profil</span></a>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="states">
          <div className="container">
            <ul className="flex">
              <li className="flex">
                <div className="numb valign">
                  <h3>9</h3>
                </div>
                <div className="text valign">
                  <p>
                    Invitations   <br />à la  Annonceur
                  </p>
                </div>
              </li>

              <li className="flex">
                <div className="numb valign">
                  <h3>35</h3>
                </div>
                <div className="text valign">
                  <p>
                    Annonceur <br /> terminées
                  </p>
                </div>
              </li>
              <li className="flex">
                <div className="numb valign">
                  <h3>8</h3>
                </div>
                <div className="text valign">
                  <p>
                    Annonceur <br /> acceptées
                  </p>
                </div>
              </li>
              <li className="flex">
                <div className="numb valign">
                  <h3>1</h3>
                </div>
                <div className="text valign">
                  <p>
                    Annonceur <br /> refusées
                  </p>
                </div>
              </li>

              <li className="mail-us">
                <a href="mailto:your@email.com?subject=Subject">
                  <div className="flex">
                    <div className="text valign">
                      <div className="full-width">
                        <p>Entrer en contact</p>
                        <h6>Vie_Support@Gmail.Com</h6>
                      </div>
                    </div>

                    <div className="mail-icon">
                      <div className="icon-box">
                        <span className="icon color-font pe-7s-mail"></span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="line bottom left"></div>
    </header>
  );
};

export default ProfilInfluenceur;
