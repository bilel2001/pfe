import React from "react";
import DarkTheme from "../../layouts/Dark";
import styles from "../../../styles/Home.module.css";
import LeftNavbar from "../../components/Admin/LeftNavbar";
import Header from "../../components/Admin/Header";
import Content from "../../components/Admin/content";
import Sidebar from "../../components/sidebar";

const Homepage1 = () => {
 
  return (
    <DarkTheme>
  <div className={styles.container}>
			
			<div className={styles.container}>
				<Sidebar />
				<Header />
				<Content />
			</div>
		</div>
    </DarkTheme>
  );
};

export default Homepage1;
