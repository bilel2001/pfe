import React from "react";
import Navbar from "../../components/Navbar/navbar";
import Intro3 from "../../components/Intro3/intro3";
import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";
import FullTestimonials from "../../components/Full-testimonials/full-testimonials";
import Team from "../../components/Team/team";
import Blogs3 from "../../components/blogs/Blogs3/blogs3";
import Services2 from "../../components/Services2/services2";
import Works2 from "../../components/Works2/works2";
import Numbers2 from "../../components/Numbers2/numbers2";
import Portfolio2 from "../../components/Portfolio2/Portfolio2";
import Clients from "../../components/Clients/clients";


import IntroAnn from "../../components/Annonceurs/introann";
import Avis from "../../components/Annonceurs/avis";
import Annonceurs from "../../components/Annonceurs/annonceurs";
import Services from "../../components/Annonceurs/services";
import Influenceurs from "../../components/Annonceurs/influenceurs";
import Recherche from "../../components/Annonceurs/recherche";



const Homepage3 = () => {
  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <IntroAnn />
      <Avis withIMG />
      <Annonceurs theme="dark" />
      <Services style="4item" />
      <Influenceurs/>
      {/* <Numbers2 /> */}
      {/* <Team /> */}
      {/* <Blogs3 /> */}
      <Recherche grid={3} filterPosition="center" />
      <Footer hideBGCOLOR />
    </DarkTheme>
  );
};

export default Homepage3;
