import React from "react";
import Sign from "../../components/Sign/Sign";
import DarkTheme from "../../layouts/Dark";

const Signup = () => {
  
    return (
      <DarkTheme>
        <Sign/>
      </DarkTheme>
    );
  };

export default Signup
