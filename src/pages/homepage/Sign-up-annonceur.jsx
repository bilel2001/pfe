import React from "react";
import Signannonceur from "../../components/Sign-annonceur/Sign-annonceur";
import DarkTheme from "../../layouts/Dark";

const Signupannonceur = () => {
  
    return (
      <DarkTheme>
        <Signannonceur/>
      </DarkTheme>
    );
  };

export default Signupannonceur

