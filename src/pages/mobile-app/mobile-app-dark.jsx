/* eslint-disable @next/next/no-page-custom-font */
import React from "react";
import DarkTheme from "../../layouts/Dark";
import NavbarMobileApp from "../../components/Navbar-mobile-app/navbar-mobile-app";
import Intro6 from "../../components/Intro6/intro6";
import Clients3 from "../../components/Clients3/clients3";
import Services7 from "../../components/Services7/services7";
import Services8 from "../../components/Services8/services8";
import Screenshots from "../../components/Screenshots/screenshots";
import Progress from "../../components/Progress/progress";
import VideoWithTeam from "../../components/Video-with-team/video-with-team";
import PricePackages from "../../components/Price-packages/price-packages";
import Testimonials from "../../components/Testimonials/testimonials";
import DownloadApp from "../../components/Download-app/download-app";
import Blogs2 from "../../components/Blogs2/blogs2";
import Footer2 from "../../components/Footer2/footer2";

const MobileAppDark = () => {
  
  return (
    
      <DarkTheme mobileappstyle>
        
        <Intro6 />
        
      </DarkTheme>
  
  );
};

export default MobileAppDark;
