import React from "react";

import DarkTheme from "../../layouts/Dark";

import LeftNavbar from "../../components/Admin/LeftNavbar";
import Header from "../../components/Admin/Header";
import Tableau from "../../components/Admin/Tableau";
import Sidebar from "../../components/Admin/sidebar";
import Widgers from "../../components/Admin/Widgers";
import Sale from "../../components/Admin/Sale";
import  Chart  from "../../components/Admin/Chart";









const Dashboard = () => {
 
  return (
    <DarkTheme>
  <Sidebar/>
  <Sale/>
  <Chart/>
  <Tableau/>
  
  
    </DarkTheme>
  );
};

export default Dashboard;
