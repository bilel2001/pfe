import React from "react";

import DarkTheme from "../../layouts/Dark";

import LeftNavbar from "../../components/Admin/LeftNavbar";
import Header from "../../components/Admin/Header";
import Tableau from "../../components/Admin/Tableau";
import Sidebarinf from "../../components/Admin/sidebarinf";
import Widgers from "../../components/Admin/Widgers";
import Sale from "../../components/Admin/Sale";
import  Chart  from "../../components/Admin/Chart";

import  StatInfluenceur  from "../../components/Admin/statinfluenceur";







const influenceursinfos = () => {
 
  return (
    <DarkTheme>
  <Sidebarinf/>
  <StatInfluenceur/>
  
    </DarkTheme>
  );
};

export default influenceursinfos;
