import React from "react";
import Navbar from "../components/Navbar/navbar";
import Footer from "../components/Footer/footer";
import DarkTheme from "../layouts/Dark";
import IntroAnn from "../components/Annonceurs/introann";
import Avis from "../components/Annonceurs/avis";
import Annonceurs from "../components/Annonceurs/annonceurs";
import Services from "../components/Annonceurs/services";
import Influenceurs from "../components/Annonceurs/influenceurs";
import Recherche from "../components/Annonceurs/recherche";



const PageAnnonceurs = () => {
  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <IntroAnn />
      <Avis withIMG />
      <Annonceurs theme="dark" />
      <Services style="4item" />
      <Influenceurs/>
      {/* <Numbers2 /> */}
      {/* <Team /> */}
      {/* <Blogs3 /> */}
      
      <Footer hideBGCOLOR />
    </DarkTheme>
  );
};

export default PageAnnonceurs;
