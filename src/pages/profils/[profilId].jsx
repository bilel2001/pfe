import React from "react";
import Navbar from "../../components/Navbar/navbar";
import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";
import FreelancreIntro from "../../components/Freelancre-intro/freelancre-intro";
import Services5 from "../../components/Services5/services5";
import WorksStyle4 from "../../components/Works-style4/works-style4";
import AboutUs5 from "../../components/About-us5/about-us5";
import SContactForm from "../../components/s-contact-form/s-contact-form";
import Clients from "../../components/Clients/clients";
import Statistiquecircle from "../../components/Skills-circle/skills-circle";
import SkillsCircle2 from "../../components/Skills-circle2/Skills-circle2";
import GrowthChart from "../../components/Growth/GrowthChart ";
import EngagementAnalysis from "../../components/Engagement/EngagementAnalysis ";
import BestPerformingPosts from "../../components/BestPerforming/BestPerformingPosts ";
import GenderChart from "../../components/Gender/GenderChart ";
import AgeAnalysis from"../../components/Age/AgeAnalysis ";
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router'
const Homepage = () => {


  const router = useRouter()

  const { profilId } = router.query
  
 



  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <FreelancreIntro profilId={profilId}/>
      < GrowthChart/>
      <GenderChart/>
      <EngagementAnalysis/>
      <AgeAnalysis/>
      <BestPerformingPosts/>
{/*<Statistiquecircle theme="dark" />
      <SkillsCircle2 theme="dark" />
      <AboutUs5 />*/}
      <WorksStyle4 />
     
      <Clients theme="dark" />
    {/*  <SContactForm noLine />*/}
      <Footer />
    </DarkTheme>
  );
};

export default Homepage;
