/* eslint-disable @next/next/no-sync-scripts */
import React from "react";
import Navbar from "../../components/Navbar/navbar";
import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";
import ContactHeader from "../../components/Contact-header/contact-header";
import ContactForm from "../../components/Contact-form/contact-form";

const Contact = () => {
  

  return (
    <DarkTheme>
     
      
      <div className="main-content">
        <ContactForm />
      </div>
    </DarkTheme>
  );
};

export default Contact;
