import React from "react";
import Navbar from "../components/Navbar/navbar";
import Footer from "../components/Footer/footer";
import DarkTheme from "../layouts/Dark";
import Avis from "../components/Annonceurs/avis";
import Annonceurs from "../components/Influenceurs/annonceurs";
import Services from "../components/Influenceurs/services";
import Influenceurs from "../components/Influenceurs/influenceurs";
import Recherche from "../components/Influenceurs/recherche";
import IntroInf from "../components/Influenceurs/introinf";



const PageInfluenceurs = () => {
  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <IntroInf />
      <Services style="4item" />
      <Avis withIMG />
      <Annonceurs theme="dark" />
      
      <Influenceurs/>
      {/* <Numbers2 /> */}
      {/* <Team /> */}
      {/* <Blogs3 /> */}
      <Recherche grid={3} filterPosition="center" />
      <Footer hideBGCOLOR />
    </DarkTheme>
  );
};

export default PageInfluenceurs;
