import React from "react";
import Navbar from "../../components/Navbar/navbar";

import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";

import Campdetails from "../../components/campagnes/campdetails";



const evenements = () => {
  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
   
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      
      <Campdetails />
     
      <Footer />
    </DarkTheme>
  );
};
 
export default evenements;
