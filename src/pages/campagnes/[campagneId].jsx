import React from "react";
import Navbar from "../../components/Navbar/navbar";

import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";

import Campdetails from "../../components/campagnes/campdetails";
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router'


const evenements = () => {
    const [campagne, setCampagne] = React.useState(null);

    const router = useRouter()
  
    const { campagneId } = router.query
    React.useEffect(() => {
      const fetchData = async () => {
        try {
  
          const res= await api.get(`campagnes/${campagneId}`);
          setCampagne(res.data)
          console.log(res.data)
          console.log(eventId)
        } catch (error) {
          console.log(error);
        }
      };
      fetchData();
    }, [campagneId]);

  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);

  React.useEffect(() => {
    var navbar = navbarRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
   
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      
      <Campdetails projectData={campagne} />
     
      <Footer />
    </DarkTheme>
  );
};
 
export default evenements;
