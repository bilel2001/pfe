import React from "react";

import DarkTheme from "../../layouts/Dark";


import Sidebar from "../../components/dashinf/sidenavbar";

import Compsata from "../../components/dashinf/Compsata";
import Dashstat from "../../components/dashinf/dashstat";









const Dashboard = () => {
 
  return (
    <DarkTheme>
  <Sidebar/>
  <Compsata/>
  
  <Dashstat/>
  
    </DarkTheme>
  );
};

export default Dashboard;
