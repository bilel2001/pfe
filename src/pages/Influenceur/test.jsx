import { useState } from "react";

const MyComponent = () => {
  const [pageUrl, setPageUrl] = useState("");
  const [pageId, setPageId] = useState("");

  const getPageId = (url) => {
    const match = url.match(/facebook\.com\/(.*)/);
    if (match) {
      const pageName = match[1].split("/")[0];

      Facebook.api(
        `/${pageName}`,
        { fields: "id" },
        (response) => {
          if (response && !response.error) {
            setPageId(response.id);
          }
        }
      );
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    getPageId(pageUrl);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={pageUrl}
          onChange={(event) => setPageUrl(event.target.value)}
        />
        <button type="submit">Obtenir l'ID de la page</button>
      </form>
      {pageId && <p>Page ID: {pageId}</p>}
    </div>
  );
};

export default MyComponent;