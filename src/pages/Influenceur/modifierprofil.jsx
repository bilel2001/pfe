import React from "react";

import DarkTheme from "../../layouts/Dark";

import LeftNavbar from "../../components/Admin/LeftNavbar";
import Header from "../../components/Admin/Header";
import Tableau from "../../components/Admin/Tableau";
import Sidebar from "../../components/dashinf/sidemodprof";
import Widgers from "../../components/Admin/Widgers";
import Sale from "../../components/Admin/Sale";
import  Chart  from "../../components/Admin/Chart";
import paramform from "../../components/dashinf/paramform"
import Editprofil from "../../components/dashinf/editprofil"






const Dashboard = () => {
 
  return (
    <DarkTheme>
  <Sidebar/>
  <Editprofil />
  
    </DarkTheme>
  );
};

export default Dashboard;
