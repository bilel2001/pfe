import React from "react";

import DarkTheme from "../../layouts/Dark";


import Sidebar from "../../components/dashinf/sidemodsoc";

import Editsocmed from "../../components/dashinf/editsocmed";







const Dashboard = () => {
 
  return (
    <DarkTheme>
  <Sidebar/>
  <Editsocmed />
  
    </DarkTheme>
  );
};

export default Dashboard;
