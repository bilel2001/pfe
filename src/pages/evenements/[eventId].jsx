import React from "react";
import Navbar from "../../components/Navbar/navbar";
import Footer from "../../components/Footer/footer";
import DarkTheme from "../../layouts/Dark";
import ProjectDetails2Header from "../../components/Project-details2-header/project-details2-header";
import ProjectDate from "../../data/events-details/recent-event-details.json";
import ProjectIntroduction from "../../components/Project-introduction/project-introduction";
import ProjectGallery from "../../components/Project-gallery/project-gallery";
import ProjectDescription from "../../components/Project-description/project-description";
import ProjectVideo from "../../components/Project-video/project-video";
import NextProject from "../../components/Next-project/next-project";
import api from "../../utils/axiosInstance";
import { useRouter } from 'next/router'

const évènementrécent = () => {
  const navbarRef = React.useRef(null);
  const logoRef = React.useRef(null);
  const [event, setEvent] = React.useState(null);

  const router = useRouter()

  const { eventId } = router.query
  React.useEffect(() => {
    const fetchData = async () => {
      try {

        const res= await api.get(`events/${eventId}`);
        setEvent(res.data)
        console.log(res.data)
        console.log(eventId)
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, [eventId]);
  React.useEffect(() => {
    var navbar = navbarRef.current,
      logo = logoRef.current;
    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });
  }, [navbarRef]);
  return (
    <DarkTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <div className="wrapper">
        <ProjectDetails2Header projectHeaderData={event} />
        <ProjectDescription projectDescriptionData={event} />
        <NextProject nextProject={event} />
        <Footer />
      </div>
    </DarkTheme>
  );
};

export default évènementrécent;
